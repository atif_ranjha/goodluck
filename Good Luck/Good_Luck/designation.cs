﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using DAL;

namespace Bakery_Management_System
{
    public partial class designation : UserControl
    {
        public designation()
        {
            InitializeComponent();
        }
        DB_Connecctions db = new DB_Connecctions();
        void bindgridview()
        {
            DataSet ds = db.Select("select * from Designation");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
        } 
        private void btn_save_Click(object sender, EventArgs e)
        {
            string query = "insert into Designation(Name,Description,Pay) values ('" + txtName.Text + "','" + txtDes.Text + "','" + txtPay.Text + "')";
            db.ExecuteQuery(query);

            MessageBox.Show("New  Data Saved Succefully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void designation_Load(object sender, EventArgs e)
        {
            bindgridview();
        }
    }
}
