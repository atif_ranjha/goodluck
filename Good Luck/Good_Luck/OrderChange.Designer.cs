﻿namespace Bakery_Management_System
{
    partial class OrderChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderChange));
            this.ProductIDColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductPriceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductQuantityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPriceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductsGridView = new System.Windows.Forms.DataGridView();
            this.DeleteColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.Increase = new System.Windows.Forms.DataGridViewImageColumn();
            this.Decrease = new System.Windows.Forms.DataGridViewImageColumn();
            this.ProductsFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label18 = new System.Windows.Forms.Label();
            this.txt_grand_total = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.CategoriesFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label24 = new System.Windows.Forms.Label();
            this.combo_table = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn3 = new System.Windows.Forms.DataGridViewImageColumn();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.Delivery_btn = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btn_process = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.SaveAdvance = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textAddress = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.txt_custname = new System.Windows.Forms.TextBox();
            this.lbl_custmoer = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnsrc2 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.ProductsGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // ProductIDColumn
            // 
            this.ProductIDColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ProductIDColumn.FillWeight = 40F;
            this.ProductIDColumn.HeaderText = "ID";
            this.ProductIDColumn.Name = "ProductIDColumn";
            this.ProductIDColumn.Width = 40;
            // 
            // ProductNameColumn
            // 
            this.ProductNameColumn.FillWeight = 103.1895F;
            this.ProductNameColumn.HeaderText = "Product Name";
            this.ProductNameColumn.Name = "ProductNameColumn";
            // 
            // ProductPriceColumn
            // 
            this.ProductPriceColumn.FillWeight = 101.0558F;
            this.ProductPriceColumn.HeaderText = "Price";
            this.ProductPriceColumn.Name = "ProductPriceColumn";
            // 
            // ProductQuantityColumn
            // 
            this.ProductQuantityColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ProductQuantityColumn.HeaderText = "Quantity";
            this.ProductQuantityColumn.Name = "ProductQuantityColumn";
            this.ProductQuantityColumn.Width = 20;
            // 
            // TotalPriceColumn
            // 
            this.TotalPriceColumn.FillWeight = 136.5847F;
            this.TotalPriceColumn.HeaderText = "Product Total";
            this.TotalPriceColumn.Name = "TotalPriceColumn";
            // 
            // Type
            // 
            this.Type.FillWeight = 22.23028F;
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            // 
            // ProductsGridView
            // 
            this.ProductsGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ProductsGridView.BackgroundColor = System.Drawing.Color.PowderBlue;
            this.ProductsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProductsGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductIDColumn,
            this.ProductNameColumn,
            this.ProductPriceColumn,
            this.ProductQuantityColumn,
            this.TotalPriceColumn,
            this.Type,
            this.DeleteColumn,
            this.Increase,
            this.Decrease});
            this.ProductsGridView.Location = new System.Drawing.Point(9, 111);
            this.ProductsGridView.Name = "ProductsGridView";
            this.ProductsGridView.Size = new System.Drawing.Size(520, 517);
            this.ProductsGridView.TabIndex = 209;
            this.ProductsGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ProductsGridView_CellContentClick);
            // 
            // DeleteColumn
            // 
            this.DeleteColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.DeleteColumn.HeaderText = "Delete";
            this.DeleteColumn.Image = global::Bakery_Management_System.Properties.Resources.Delete_26px;
            this.DeleteColumn.Name = "DeleteColumn";
            this.DeleteColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DeleteColumn.Width = 45;
            // 
            // Increase
            // 
            this.Increase.FillWeight = 69.78568F;
            this.Increase.HeaderText = "Add";
            this.Increase.Image = global::Bakery_Management_System.Properties.Resources.Plus_Math_20px;
            this.Increase.Name = "Increase";
            this.Increase.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Decrease
            // 
            this.Decrease.FillWeight = 69.78568F;
            this.Decrease.HeaderText = "Subtract";
            this.Decrease.Image = global::Bakery_Management_System.Properties.Resources.Subtract_26px;
            this.Decrease.Name = "Decrease";
            this.Decrease.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // ProductsFlowPanel
            // 
            this.ProductsFlowPanel.AutoScroll = true;
            this.ProductsFlowPanel.BackColor = System.Drawing.Color.PowderBlue;
            this.ProductsFlowPanel.Location = new System.Drawing.Point(542, 131);
            this.ProductsFlowPanel.Name = "ProductsFlowPanel";
            this.ProductsFlowPanel.Size = new System.Drawing.Size(798, 218);
            this.ProductsFlowPanel.TabIndex = 217;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(116, 9);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(123, 25);
            this.label18.TabIndex = 213;
            this.label18.Text = "Printer name";
            this.label18.Visible = false;
            // 
            // txt_grand_total
            // 
            this.txt_grand_total.BackColor = System.Drawing.Color.PowderBlue;
            this.txt_grand_total.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_grand_total.Enabled = false;
            this.txt_grand_total.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25743F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_grand_total.ForeColor = System.Drawing.Color.Red;
            this.txt_grand_total.Location = new System.Drawing.Point(167, 654);
            this.txt_grand_total.Multiline = true;
            this.txt_grand_total.Name = "txt_grand_total";
            this.txt_grand_total.Size = new System.Drawing.Size(85, 29);
            this.txt_grand_total.TabIndex = 200;
            this.txt_grand_total.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 649);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 30);
            this.label6.TabIndex = 201;
            this.label6.Text = "Grand Total :";
            // 
            // CategoriesFlowPanel
            // 
            this.CategoriesFlowPanel.AutoScroll = true;
            this.CategoriesFlowPanel.BackColor = System.Drawing.Color.PowderBlue;
            this.CategoriesFlowPanel.Location = new System.Drawing.Point(542, 353);
            this.CategoriesFlowPanel.Name = "CategoriesFlowPanel";
            this.CategoriesFlowPanel.Size = new System.Drawing.Size(798, 371);
            this.CategoriesFlowPanel.TabIndex = 195;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(54, 47);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(56, 25);
            this.label24.TabIndex = 189;
            this.label24.Text = "Table";
            // 
            // combo_table
            // 
            this.combo_table.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combo_table.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combo_table.BackColor = System.Drawing.Color.WhiteSmoke;
            this.combo_table.Enabled = false;
            this.combo_table.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_table.FormattingEnabled = true;
            this.combo_table.Location = new System.Drawing.Point(116, 49);
            this.combo_table.Name = "combo_table";
            this.combo_table.Size = new System.Drawing.Size(188, 26);
            this.combo_table.TabIndex = 188;
            this.combo_table.SelectedIndexChanged += new System.EventHandler(this.combo_table_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(33, 12);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 25);
            this.label13.TabIndex = 187;
            this.label13.Text = "Order #";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewImageColumn1.HeaderText = "Delete";
            this.dataGridViewImageColumn1.Image = global::Bakery_Management_System.Properties.Resources.Delete_26px;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.Width = 45;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.FillWeight = 69.78568F;
            this.dataGridViewImageColumn2.HeaderText = "Add";
            this.dataGridViewImageColumn2.Image = global::Bakery_Management_System.Properties.Resources.Plus_Math_20px;
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn2.Width = 51;
            // 
            // dataGridViewImageColumn3
            // 
            this.dataGridViewImageColumn3.FillWeight = 69.78568F;
            this.dataGridViewImageColumn3.HeaderText = "Subtract";
            this.dataGridViewImageColumn3.Image = global::Bakery_Management_System.Properties.Resources.Subtract_26px;
            this.dataGridViewImageColumn3.Name = "dataGridViewImageColumn3";
            this.dataGridViewImageColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn3.Width = 52;
            // 
            // SaveBtn
            // 
            this.SaveBtn.BackColor = System.Drawing.Color.MediumPurple;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveBtn.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveBtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.SaveBtn.Image = ((System.Drawing.Image)(resources.GetObject("SaveBtn.Image")));
            this.SaveBtn.Location = new System.Drawing.Point(1214, 7);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(126, 52);
            this.SaveBtn.TabIndex = 199;
            this.SaveBtn.Text = "Save Order";
            this.SaveBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.SaveBtn.UseVisualStyleBackColor = false;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // Delivery_btn
            // 
            this.Delivery_btn.BackColor = System.Drawing.Color.RoyalBlue;
            this.Delivery_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Delivery_btn.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Delivery_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Delivery_btn.Image = ((System.Drawing.Image)(resources.GetObject("Delivery_btn.Image")));
            this.Delivery_btn.Location = new System.Drawing.Point(1081, 7);
            this.Delivery_btn.Name = "Delivery_btn";
            this.Delivery_btn.Size = new System.Drawing.Size(126, 52);
            this.Delivery_btn.TabIndex = 212;
            this.Delivery_btn.Text = "Delivery";
            this.Delivery_btn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Delivery_btn.UseVisualStyleBackColor = false;
            this.Delivery_btn.Click += new System.EventHandler(this.Delivery_btn_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.RoyalBlue;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(1214, 75);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(126, 52);
            this.button2.TabIndex = 219;
            this.button2.Text = "Add Bill";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btn_process
            // 
            this.btn_process.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_process.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_process.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_process.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btn_process.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_process.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_process.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_process.Image = ((System.Drawing.Image)(resources.GetObject("btn_process.Image")));
            this.btn_process.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_process.Location = new System.Drawing.Point(817, 76);
            this.btn_process.Name = "btn_process";
            this.btn_process.Size = new System.Drawing.Size(126, 52);
            this.btn_process.TabIndex = 203;
            this.btn_process.Text = "Print Bill";
            this.btn_process.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_process.UseVisualStyleBackColor = false;
            this.btn_process.Click += new System.EventHandler(this.btn_process_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.SkyBlue;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Image = global::Bakery_Management_System.Properties.Resources.Cancel_30px;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(949, 76);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 52);
            this.button1.TabIndex = 216;
            this.button1.Text = "Cancel";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.DarkGreen;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(1081, 76);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(126, 52);
            this.button4.TabIndex = 207;
            this.button4.Text = "Payment";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // SaveAdvance
            // 
            this.SaveAdvance.BackColor = System.Drawing.Color.LightSeaGreen;
            this.SaveAdvance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveAdvance.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveAdvance.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.SaveAdvance.Image = ((System.Drawing.Image)(resources.GetObject("SaveAdvance.Image")));
            this.SaveAdvance.Location = new System.Drawing.Point(685, 76);
            this.SaveAdvance.Name = "SaveAdvance";
            this.SaveAdvance.Size = new System.Drawing.Size(126, 52);
            this.SaveAdvance.TabIndex = 211;
            this.SaveAdvance.Text = "Advance";
            this.SaveAdvance.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.SaveAdvance.UseVisualStyleBackColor = false;
            this.SaveAdvance.Click += new System.EventHandler(this.SaveAdvance_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(446, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 25);
            this.label3.TabIndex = 230;
            this.label3.Text = "Address";
            // 
            // textAddress
            // 
            this.textAddress.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAddress.Location = new System.Drawing.Point(526, 47);
            this.textAddress.Name = "textAddress";
            this.textAddress.Size = new System.Drawing.Size(194, 24);
            this.textAddress.TabIndex = 229;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(732, 47);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 25);
            this.label10.TabIndex = 225;
            this.label10.Text = "Phone #";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(817, 46);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(194, 24);
            this.textBox4.TabIndex = 224;
            // 
            // txt_custname
            // 
            this.txt_custname.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txt_custname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_custname.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_custname.Location = new System.Drawing.Point(526, 16);
            this.txt_custname.Name = "txt_custname";
            this.txt_custname.Size = new System.Drawing.Size(194, 24);
            this.txt_custname.TabIndex = 223;
            // 
            // lbl_custmoer
            // 
            this.lbl_custmoer.AutoSize = true;
            this.lbl_custmoer.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_custmoer.Location = new System.Drawing.Point(374, 16);
            this.lbl_custmoer.Name = "lbl_custmoer";
            this.lbl_custmoer.Size = new System.Drawing.Size(152, 25);
            this.lbl_custmoer.TabIndex = 222;
            this.lbl_custmoer.Text = "Customer Name";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(116, 13);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(188, 29);
            this.textBox1.TabIndex = 233;
            // 
            // btnsrc2
            // 
            this.btnsrc2.FlatAppearance.BorderSize = 0;
            this.btnsrc2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnsrc2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnsrc2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsrc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsrc2.Image = global::Bakery_Management_System.Properties.Resources.Search_30px;
            this.btnsrc2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsrc2.Location = new System.Drawing.Point(310, 12);
            this.btnsrc2.Name = "btnsrc2";
            this.btnsrc2.Size = new System.Drawing.Size(48, 35);
            this.btnsrc2.TabIndex = 234;
            this.btnsrc2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsrc2.UseVisualStyleBackColor = true;
            this.btnsrc2.Click += new System.EventHandler(this.btnsrc2_Click_1);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(804, 12);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(133, 25);
            this.label21.TabIndex = 236;
            this.label21.Text = "Order Detail:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CalendarFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(938, 10);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(137, 33);
            this.dateTimePicker1.TabIndex = 235;
            // 
            // OrderChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.ClientSize = new System.Drawing.Size(1346, 723);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.btnsrc2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textAddress);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.txt_custname);
            this.Controls.Add(this.lbl_custmoer);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.Delivery_btn);
            this.Controls.Add(this.ProductsGridView);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.ProductsFlowPanel);
            this.Controls.Add(this.btn_process);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.SaveAdvance);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txt_grand_total);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.CategoriesFlowPanel);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.combo_table);
            this.Controls.Add(this.label13);
            this.Name = "OrderChange";
            this.Text = "OrderChange";
            this.Load += new System.EventHandler(this.OrderChange_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ProductsGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Button Delivery_btn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductIDColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductPriceColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductQuantityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalPriceColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewImageColumn DeleteColumn;
        private System.Windows.Forms.DataGridViewImageColumn Increase;
        private System.Windows.Forms.DataGridViewImageColumn Decrease;
        public System.Windows.Forms.DataGridView ProductsGridView;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.FlowLayoutPanel ProductsFlowPanel;
        private System.Windows.Forms.Button btn_process;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button SaveAdvance;
        private System.Windows.Forms.Label label18;
        public System.Windows.Forms.TextBox txt_grand_total;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FlowLayoutPanel CategoriesFlowPanel;
        private System.Windows.Forms.Label label24;
        public System.Windows.Forms.ComboBox combo_table;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textAddress;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox textBox4;
        public System.Windows.Forms.TextBox txt_custname;
        private System.Windows.Forms.Label lbl_custmoer;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnsrc2;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}