﻿namespace Bakery_Management_System
{
    partial class Statistics
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_totl_sales = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_tot_cash = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.combo_month = new System.Windows.Forms.ComboBox();
            this.combo_year = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_ser = new System.Windows.Forms.Button();
            this.btn_allSale = new System.Windows.Forms.Button();
            this.btn_annual = new System.Windows.Forms.Button();
            this.btn_monthly = new System.Windows.Forms.Button();
            this.btn_daily = new System.Windows.Forms.Button();
            this.btnsrc2 = new System.Windows.Forms.Button();
            this.btnsrc3 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.PowderBlue;
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1233, 60);
            this.panel1.TabIndex = 29;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(473, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(199, 32);
            this.label7.TabIndex = 1;
            this.label7.Text = "Reports Section";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(86, 210);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(912, 317);
            this.dataGridView1.TabIndex = 39;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Black", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1054, 232);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 21);
            this.label3.TabIndex = 41;
            this.label3.Text = "Total Sales :";
            // 
            // txt_totl_sales
            // 
            this.txt_totl_sales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_totl_sales.Enabled = false;
            this.txt_totl_sales.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_totl_sales.Location = new System.Drawing.Point(1048, 274);
            this.txt_totl_sales.Name = "txt_totl_sales";
            this.txt_totl_sales.Size = new System.Drawing.Size(137, 26);
            this.txt_totl_sales.TabIndex = 40;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Black", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1057, 317);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 21);
            this.label1.TabIndex = 43;
            this.label1.Text = "Total Cash :";
            // 
            // txt_tot_cash
            // 
            this.txt_tot_cash.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_tot_cash.Enabled = false;
            this.txt_tot_cash.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_tot_cash.Location = new System.Drawing.Point(1048, 358);
            this.txt_tot_cash.Name = "txt_tot_cash";
            this.txt_tot_cash.Size = new System.Drawing.Size(137, 26);
            this.txt_tot_cash.TabIndex = 42;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "MM/dd/yyyy";
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(101, 160);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(170, 26);
            this.dateTimePicker1.TabIndex = 44;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(135, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 21);
            this.label2.TabIndex = 45;
            this.label2.Text = "Select Date";
            // 
            // combo_month
            // 
            this.combo_month.BackColor = System.Drawing.Color.Beige;
            this.combo_month.DisplayMember = "1";
            this.combo_month.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_month.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.combo_month.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_month.FormattingEnabled = true;
            this.combo_month.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.combo_month.Location = new System.Drawing.Point(379, 160);
            this.combo_month.Name = "combo_month";
            this.combo_month.Size = new System.Drawing.Size(159, 26);
            this.combo_month.TabIndex = 46;
            this.combo_month.ValueMember = "1";
            // 
            // combo_year
            // 
            this.combo_year.BackColor = System.Drawing.Color.Beige;
            this.combo_year.DisplayMember = "1";
            this.combo_year.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_year.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.combo_year.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_year.FormattingEnabled = true;
            this.combo_year.Items.AddRange(new object[] {
            "2015",
            "2016",
            "2017",
            "2018",
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025",
            "2026",
            "2027",
            "2028",
            "2029",
            "2030"});
            this.combo_year.Location = new System.Drawing.Point(663, 160);
            this.combo_year.Name = "combo_year";
            this.combo_year.Size = new System.Drawing.Size(159, 26);
            this.combo_year.TabIndex = 47;
            this.combo_year.ValueMember = "1";
            this.combo_year.SelectedIndexChanged += new System.EventHandler(this.combo_year_SelectedIndexChanged);
            this.combo_year.SelectedValueChanged += new System.EventHandler(this.combo_year_SelectedValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(398, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 21);
            this.label4.TabIndex = 48;
            this.label4.Text = "Select Month";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(691, 131);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 21);
            this.label5.TabIndex = 49;
            this.label5.Text = "Select Year";
            // 
            // btn_ser
            // 
            this.btn_ser.FlatAppearance.BorderSize = 0;
            this.btn_ser.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_ser.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btn_ser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ser.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ser.Image = global::Bakery_Management_System.Properties.Resources.Search_30px;
            this.btn_ser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_ser.Location = new System.Drawing.Point(833, 153);
            this.btn_ser.Name = "btn_ser";
            this.btn_ser.Size = new System.Drawing.Size(48, 35);
            this.btn_ser.TabIndex = 50;
            this.btn_ser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_ser.UseVisualStyleBackColor = true;
            this.btn_ser.Click += new System.EventHandler(this.btn_ser_Click);
            // 
            // btn_allSale
            // 
            this.btn_allSale.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_allSale.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btn_allSale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_allSale.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_allSale.Image = global::Bakery_Management_System.Properties.Resources.Planner_30px;
            this.btn_allSale.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_allSale.Location = new System.Drawing.Point(219, 75);
            this.btn_allSale.Name = "btn_allSale";
            this.btn_allSale.Size = new System.Drawing.Size(123, 42);
            this.btn_allSale.TabIndex = 38;
            this.btn_allSale.Text = "All Sales";
            this.btn_allSale.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_allSale.UseVisualStyleBackColor = true;
            this.btn_allSale.Click += new System.EventHandler(this.btn_allSale_Click);
            // 
            // btn_annual
            // 
            this.btn_annual.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_annual.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btn_annual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_annual.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_annual.Image = global::Bakery_Management_System.Properties.Resources.Calendar_12_30px;
            this.btn_annual.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_annual.Location = new System.Drawing.Point(737, 75);
            this.btn_annual.Name = "btn_annual";
            this.btn_annual.Size = new System.Drawing.Size(149, 42);
            this.btn_annual.TabIndex = 37;
            this.btn_annual.Text = "Annual Sale";
            this.btn_annual.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_annual.UseVisualStyleBackColor = true;
            this.btn_annual.Click += new System.EventHandler(this.btn_annual_Click);
            // 
            // btn_monthly
            // 
            this.btn_monthly.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_monthly.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btn_monthly.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_monthly.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_monthly.Image = global::Bakery_Management_System.Properties.Resources.Calendar_31_30px;
            this.btn_monthly.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_monthly.Location = new System.Drawing.Point(547, 75);
            this.btn_monthly.Name = "btn_monthly";
            this.btn_monthly.Size = new System.Drawing.Size(155, 42);
            this.btn_monthly.TabIndex = 36;
            this.btn_monthly.Text = "Monthly Sale";
            this.btn_monthly.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_monthly.UseVisualStyleBackColor = true;
            this.btn_monthly.Click += new System.EventHandler(this.btn_monthly_Click);
            // 
            // btn_daily
            // 
            this.btn_daily.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_daily.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btn_daily.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_daily.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_daily.Image = global::Bakery_Management_System.Properties.Resources.Calendar_24_30px;
            this.btn_daily.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_daily.Location = new System.Drawing.Point(379, 75);
            this.btn_daily.Name = "btn_daily";
            this.btn_daily.Size = new System.Drawing.Size(131, 42);
            this.btn_daily.TabIndex = 35;
            this.btn_daily.Text = "Daily Sale";
            this.btn_daily.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_daily.UseVisualStyleBackColor = true;
            this.btn_daily.Click += new System.EventHandler(this.btn_daily_Click);
            // 
            // btnsrc2
            // 
            this.btnsrc2.FlatAppearance.BorderSize = 0;
            this.btnsrc2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnsrc2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnsrc2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsrc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsrc2.Image = global::Bakery_Management_System.Properties.Resources.Search_30px;
            this.btnsrc2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsrc2.Location = new System.Drawing.Point(294, 155);
            this.btnsrc2.Name = "btnsrc2";
            this.btnsrc2.Size = new System.Drawing.Size(48, 35);
            this.btnsrc2.TabIndex = 51;
            this.btnsrc2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsrc2.UseVisualStyleBackColor = true;
            this.btnsrc2.Click += new System.EventHandler(this.btnsrc2_Click);
            // 
            // btnsrc3
            // 
            this.btnsrc3.FlatAppearance.BorderSize = 0;
            this.btnsrc3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnsrc3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnsrc3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsrc3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsrc3.Image = global::Bakery_Management_System.Properties.Resources.Search_30px;
            this.btnsrc3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsrc3.Location = new System.Drawing.Point(583, 155);
            this.btnsrc3.Name = "btnsrc3";
            this.btnsrc3.Size = new System.Drawing.Size(48, 35);
            this.btnsrc3.TabIndex = 52;
            this.btnsrc3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsrc3.UseVisualStyleBackColor = true;
            this.btnsrc3.Click += new System.EventHandler(this.btnsrc3_Click);
            // 
            // Statistics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnsrc3);
            this.Controls.Add(this.btnsrc2);
            this.Controls.Add(this.btn_ser);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.combo_year);
            this.Controls.Add(this.combo_month);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_tot_cash);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_totl_sales);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btn_allSale);
            this.Controls.Add(this.btn_annual);
            this.Controls.Add(this.btn_monthly);
            this.Controls.Add(this.btn_daily);
            this.Controls.Add(this.panel1);
            this.Name = "Statistics";
            this.Size = new System.Drawing.Size(1233, 552);
            this.Load += new System.EventHandler(this.Statistics_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_daily;
        private System.Windows.Forms.Button btn_monthly;
        private System.Windows.Forms.Button btn_annual;
        private System.Windows.Forms.Button btn_allSale;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_totl_sales;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_tot_cash;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox combo_month;
        private System.Windows.Forms.ComboBox combo_year;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_ser;
        private System.Windows.Forms.Button btnsrc2;
        private System.Windows.Forms.Button btnsrc3;
    }
}
