﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace Bakery_Management_System
{
    public partial class Admin : UserControl
    {
        public Admin()
        {
            InitializeComponent();
        }
        // *****+++++++++++++++ DATABASE CONNECTION CLASS GLOBAL ++++++++++++*******//
        DB_Connecctions db = new DB_Connecctions();
        // *****+++++++++++++++ TO REFRESH GRID VIEW ++++++++++++*******//
        public void get_id()
        {
            DataSet ds = db.Select("select isnull(max(cast(User_ID as int)),0)+1 from Admin");
            txtID.Text = ds.Tables["tbl1"].Rows[0][0].ToString();
        }
        public void refre()
        {
            DataSet ds = db.Select("select * from Admin");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
        }
        
        // *****+++++++++++++++ TO Clear Form ++++++++++++*******//
        public void clear()
        {
            txtID.Clear();
            txt_firstname.Clear();
            txt_lastname.Clear();
            txt_username.Clear();
            txt_password.Clear();
            mask_cnic.Clear();
        }
        //***+++++++ TO GET PRINTERS LSIT /////
        public void printer_list()
        {
            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                comboBox1.Items.Add(printer);
            }
        }
        //**** PRINTER NAME IN GRID VIEW CODE///*** ++++++
        public void print_gridview()
        {
            
            DataSet ds = db.Select("select * from printer");
            dataGridView2.DataSource = ds.Tables["tbl1"].DefaultView;
        }
        // *****+++++++++++++++ TO Clear Form ++++++++++++*******//
        private void Admin_Load(object sender, EventArgs e)
        {
            refre();// TO LOAD DATA IN GRIDview
            printer_list();//*** TO GET PRINTERS LIST
            print_gridview();//*** TO SHOW PRINTER IN GRIDVIEW
            get_id();
            btn_process.Enabled = true;
            btn_update.Enabled = false;
            btn_delete.Enabled = false;
        }
        // *****+++++++++++++++ SAVE DATA BUTTON ++++++++++++*******//
        private void btn_process_Click(object sender, EventArgs e)
        {
            if(txt_firstname.Text=="" || txt_lastname.Text=="" || txt_username.Text=="" || txt_password.Text=="")
            {
                MessageBox.Show("Please! Fill all the Fields.","Admin",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    db.ExecuteQuery("insert into admin(first_name,Last_name,DOB,CNIC,User_name,Password,Status) values('" + txt_firstname.Text + "','" + txt_lastname.Text + "','" + dateTimePicker1.Value + "','" + mask_cnic.Text + "','" + txt_username.Text + "','" + txt_password.Text + "','" + combo_status.Text + "')");
                    MessageBox.Show("New Admin Saved Succefully.", "Admin Section", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    clear();
                    refre();
                }
                catch (Exception)
                {

                }
            }
            
        }
        // *****+++++++++++++++ Update DATA BUTTON ++++++++++++*******//
        private void btn_update_Click(object sender, EventArgs e)
        {
            try
            {
                db.ExecuteQuery("update admin set first_name='" + txt_firstname.Text + "',last_name='" + txt_lastname.Text + "',DOB='" + dateTimePicker1.Value + "',CNIC='" + mask_cnic.Text + "',user_name='" + txt_username.Text + "',password='" + txt_password.Text + "',status='" + combo_status.Text + "' where User_ID='" + txtID.Text + "'");
                MessageBox.Show("Admin Updated Succefully.", "Admin Section", MessageBoxButtons.OK, MessageBoxIcon.Information);
                clear();
                refre();
                btn_process.Enabled = true;
                btn_update.Enabled = false;
                btn_delete.Enabled = false;
            }
            catch (Exception)
            {

            }
        }
        // *****+++++++++++++++ DELETE DATA BUTTON ++++++++++++*******//
        private void btn_delete_Click(object sender, EventArgs e)
        {
            try
            {
                db.ExecuteQuery("delete from admin where User_ID='" + txtID.Text + "'");
                MessageBox.Show("Admin Deleted Succefully.", "Admin Section", MessageBoxButtons.OK, MessageBoxIcon.Information);
                clear();
                refre();
                btn_process.Enabled = true;
                btn_update.Enabled = false;
                btn_delete.Enabled = false;
            }
            catch (Exception)
            {

            }
        }
        // *****+++++++++++++++ SEARCH DATA FROM GRIDVIEW ++++++++++++*******//
        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            DataSet ds = db.Select("select * from Admin where user_name like '%" + txt_search.Text + "%' or first_name like '%" + txt_search.Text + "%'");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
        }
       
        // *****+++++++++++++++ To Clear  the Form  ++++++++++++*******//
        private void btn_clear_Click(object sender, EventArgs e)
        {
            clear();
            refre();
            btn_process.Enabled = true;
            btn_delete.Enabled = false;
            btn_update.Enabled = false;
        }
        //*** ++++ PRINTER NAme Save  BUTOON CODE /**++++
        private void button1_Click(object sender, EventArgs e)
        {
            db.ExecuteQuery("update printer set Name='"+comboBox1.Text+"' where printer_ID='1'");
            MessageBox.Show("Printer Saved .","Printer",MessageBoxButtons.OK,MessageBoxIcon.Information);
            print_gridview();// Refresh GridView
        }
        // *****+++++++++++++++ DATAGRID VIEW ON CELL CLICK FORM FILL ++++++++++++*******//
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                txt_firstname.Text = row.Cells[0].Value.ToString();
                txt_lastname.Text = row.Cells[1].Value.ToString();
                dateTimePicker1.Value = Convert.ToDateTime(row.Cells[2].Value.ToString());
                mask_cnic.Text = row.Cells[3].Value.ToString();
                txt_username.Text = row.Cells[4].Value.ToString();
                txt_password.Text = row.Cells[5].Value.ToString();
                combo_status.Text = row.Cells[6].Value.ToString();
                textBox1.Text = row.Cells[4].Value.ToString();
            }
            btn_process.Enabled = false;
            btn_update.Enabled = true;
            btn_delete.Enabled = true;
        }              
    }
}
