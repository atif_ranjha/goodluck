﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace Bakery_Management_System
{
    public partial class Stock : UserControl
    {
        public Stock()
        {
            InitializeComponent();
        }
        // *****+++++++++++++++ DATABASE CONNECTION CLASS GLOBAL ++++++++++++*******//
        DB_Connecctions db = new DB_Connecctions();
        // *****+++++++++++++++ TO REFRESH GRID VIEW ++++++++++++*******//
        public void refre()
        {
            DataSet ds = db.Select("select [Item_Code],[Item_Name],[Item_Description],[Unit],[Item_Stock],Item_Price as 'Low Stock Limit' from Items");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
        }
       
        // *****+++++++++++++++ TO Clear Form ++++++++++++*******//
        public void clear()
        {
            txt_discription.Clear();
            txt_item_Code.Clear();
            txt_item_name.Clear();
            txt_stock.Clear();
            txt_unit.Clear();
        }
        //**+++++ TO SHOW LOW STOCK WARNING //++++++*****
        public void low_stock()
        {
            DataSet ds = db.Select("select * from items where Item_Stock<=Item_Price ");
            if (ds != null && ds.Tables["tbl1"].Rows.Count > 0)
            {
                label10.ForeColor = System.Drawing.Color.Red;
                label10.Text = "Warning! Items are at low of stock";
            }
            else
            {
                label10.Text = "";
            }
            
        }
        //***** TO SHOW LOW STOCK ITEMS IN  GRID VIEW ///******
        public void low_item()
        {
            DataSet ds = db.Select("select [Item_Code],[Item_Name],[Item_Description],[Unit],[Item_Stock],Item_Price as 'Low Stock Limit' from items where Item_Stock<=Item_Price ");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
        }
        // *****+++++++++++++++ FORM LOAD EVENTS AND FUNCTIONS ++++++++++++*******//
        private void Stock_Load(object sender, EventArgs e)
        {
            low_stock();//** TO SHOW LOW STOCK LABL
            if (label10.Text == "Warning! Items are at low of stock")
            {
                low_item();
            }
            else
            {
                refre();// TO Load Gridview on Form Load
            }
            
            
        }
        // *****+++++++++++++++ Update Stock Button ++++++++++++*******//
        private void btn_save_Click(object sender, EventArgs e)
        {
            //try
            //{
                db.ExecuteQuery("update items set item_stock=item_stock+'"+txt_update_stock.Text+"' where item_code='"+txt_item_Code.Text+"'");
                MessageBox.Show("Stock Updated Successfully.", "Stock Section", MessageBoxButtons.OK, MessageBoxIcon.Information);
                clear();
                refre();
                low_stock();
                txt_update_stock.Clear();
            //}
            //catch (Exception)
            //{
            //}
        }
        // *****+++++++++++++++ Search ITEMS from GridView ++++++++++++*******//
        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            DataSet ds = db.Select("select [Item_Code],[Item_Name],[Item_Description],[Unit],[Item_Stock],Item_Prics as 'Low Stock Limit' from Items where item_code like '%" + txt_search.Text + "%' or item_name like '%" + txt_search.Text + "%'");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
        }
        // *****+++++++++++++++ GRID VIEW CELL CLICK TO FILL FORM ++++++++++++*******//
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                txt_item_Code.Text = row.Cells[0].Value.ToString();
                txt_item_name.Text = row.Cells[1].Value.ToString();
                txt_discription.Text = row.Cells[2].Value.ToString();
                txt_unit.Text = row.Cells[3].Value.ToString();
                txt_stock.Text = row.Cells[4].Value.ToString();
            }
        }
        //***++ LOW STOCK BUTTON CODE //**++++++++++++
        private void button1_Click(object sender, EventArgs e)
        {
            low_item();
        }
        //*** TO REFRESH BUTTON //**+++++++
        private void button2_Click(object sender, EventArgs e)
        {
            refre();
        }
    }
}
