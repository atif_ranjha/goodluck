﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace Bakery_Management_System
{
    public partial class Statistics : UserControl
    {
        public Statistics()
        {
            InitializeComponent();
        }
        int a;
        //*** GLOBAL VARIABLE /////////
        DB_Connecctions db = new DB_Connecctions();
        //** +++*** rEFRESH Onload Sale Record //****
        public void refre()
        {
            DataSet ds = db.Select("Select Sale_ID,Item_code,Item_Name,Quantity,Price,BILL,Date from sale where date='" + dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss") + "' and payment=1");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
            DataSet cou = db.Select("select count(sale_ID) from sale where date='" + dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss") + "' and payment=1");
            txt_totl_sales.Text = cou.Tables["tbl1"].Rows[0][0].ToString();
            DataSet cash = db.Select("select SUM(Bill) from sale where date='" + dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss") + "' and payment=1");
            txt_tot_cash.Text = cash.Tables["tbl1"].Rows[0][0].ToString();
        }
        //*** on load events //*** +++++++++++++++
        private void Statistics_Load(object sender, EventArgs e)
        {
            dateTimePicker1.Value = System.DateTime.Today;//*** TO GET TODAY DATE
            refre();
            //month_ser();
            //** TO HIDE CONTROLS //** ++++
            dateTimePicker1.Visible = false;
            combo_month.Visible = false;
            combo_year.Visible = false;
            btn_ser.Visible = false;
            btnsrc2.Visible = false;
            btnsrc3.Visible = false;
        }
        //***** +++++ BUTTON ALL SALE //++++++++++++++++++
        private void btn_allSale_Click(object sender, EventArgs e)
        {
            //** TO HIDE CONTROLS //** ++++
            dateTimePicker1.Visible = false;
            combo_month.Visible = false;
            combo_year.Visible = false;
            btn_ser.Visible = false;
            btnsrc2.Visible = false;
            btnsrc3.Visible = false;
            ///***** REOCRD //***++++
            DataSet ds = db.Select("Select Sale_ID,Item_code,Item_Name,Quantity,Price,BILL,Date from sale");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
            DataSet cou = db.Select("select count(sale_ID) from sale ");
            txt_totl_sales.Text = cou.Tables["tbl1"].Rows[0][0].ToString();
            DataSet cash = db.Select("select SUM(Bill) from sale");
            txt_tot_cash.Text = cash.Tables["tbl1"].Rows[0][0].ToString();

        }
        //**** DAILY SALE THROUGH CALA+ENDER  ///**++++++
        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            dateTimePicker1.Visible = true;
            refre();

        }
        //*** DAILY SAALE BUTTON /******
        private void btn_daily_Click(object sender, EventArgs e)
        {
            //** TO HIDE CONTROLS //** ++++
            dateTimePicker1.Visible = true;
            btnsrc2.Visible = true;
            combo_month.Visible = false;
            combo_year.Visible = false;
            btn_ser.Visible = false;
            btnsrc3.Visible = false;
            refre();
        }
        //*** ++++++ BUTTON MONTHLY CODE ///*++++++
        private void btn_monthly_Click(object sender, EventArgs e)
        {
            //** TO HIDE CONTROLS //** ++++
            dateTimePicker1.Visible = false;
            combo_month.Visible =true;
            combo_year.Visible = true;
            btnsrc3.Visible = true;
            btn_ser.Visible = false;
            btnsrc2.Visible = false;
            
        }
        //**** FUNCTION CODE FOR MONTHLY SEARCH ///++++++
        public void month_ser()
        {
            //DataSet ds = db.Select("Select Sale_ID,Item_code,Item_Name,Quantity,Price,BILL,Date from sale where MONTH(date)='" + a + "' and Year(date)='" + combo_year.Text + "' ");
            //dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
            //DataSet cou = db.Select("select count(sale_ID) from sale where MONTH(date)='" + a + "' and Year(date)='" + combo_year.Text + "'");
            //txt_totl_sales.Text = cou.Tables["tbl1"].Rows[0][0].ToString();
            //DataSet cash = db.Select("select SUM(Bill) from sale where MONTH(date)='" + a + "' and Year(date)='" + combo_year.Text + "' ");
            //txt_tot_cash.Text = cash.Tables["tbl1"].Rows[0][0].ToString();
        }
        //*** ++++ ANNUAL SALE BUTTON ///*****++++++
        private void btn_annual_Click(object sender, EventArgs e)
        {
            //** TO HIDE CONTROLS //** ++++
            dateTimePicker1.Visible = false;
            combo_month.Visible = false;
            combo_year.Visible = true;
            btn_ser.Visible = true;
            btnsrc2.Visible = false;
            btnsrc3.Visible = false;
        }
        //**++++++++++++ ANNUAL SEARCH BUTTON CODE  ///************
        private void btn_ser_Click(object sender, EventArgs e)
        {
            DataSet ds = db.Select("Select Sale_ID,Item_code,Item_Name,Quantity,Price,BILL,Date from sale where Year(date)='" + combo_year.Text + "' ");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
            DataSet cou = db.Select("select count(sale_ID) from sale where Year(date)='" + combo_year.Text + "'");
            txt_totl_sales.Text = cou.Tables["tbl1"].Rows[0][0].ToString();
            DataSet cash = db.Select("select SUM(Bill) from sale where Year(date)='" + combo_year.Text + "' ");
            txt_tot_cash.Text = cash.Tables["tbl1"].Rows[0][0].ToString();
        }
        //***+++++++ MONTHLY SEARCH COMOBOX //**+++++++++++++
        private void combo_year_SelectedValueChanged(object sender, EventArgs e)
        {
            //if (combo_month.Text == "January")
            //{
            //    a = 01;
            //    month_ser();
            //}
            //else if (combo_month.Text == "February")
            //{
            //    a = 02;
            //    month_ser();
            //}
            //else if (combo_month.Text == "March")
            //{
            //    a = 03;
            //    month_ser();
            //}
            //else if (combo_month.Text == "April")
            //{
            //    a = 04;
            //    month_ser();
            //}
            //else if (combo_month.Text == "May")
            //{
            //    a = 05;
            //    month_ser();
            //}
            //else if (combo_month.Text == "June")
            //{
            //    a = 06;
            //    month_ser();
            //}
            //else if (combo_month.Text == "July")
            //{
            //    a = 07;
            //    month_ser();
            //}
            //else if (combo_month.Text == "Agust")
            //{
            //    a = 08;
            //    month_ser();
            //}
            //else if (combo_month.Text == "September")
            //{
            //    a = 09;
            //    month_ser();
            //}
            //else if (combo_month.Text == "October")
            //{
            //    a = 10;
            //    month_ser();
            //}
            //else if (combo_month.Text == "November")
            //{
            //    a = 11;
            //    month_ser();
            //}
            //else
            //{
            //    a = 12;
            //    month_ser();
            //}
        }

        private void btnsrc2_Click(object sender, EventArgs e)
        {
            DataSet ds = db.Select("Select Sale_ID,Item_code,Item_Name,Quantity,Price,BILL,Date from sale where Date='" + dateTimePicker1.Value + "' ");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
            DataSet cou = db.Select("select count(sale_ID) from sale where Date='" + dateTimePicker1.Value + "'");
            txt_totl_sales.Text = cou.Tables["tbl1"].Rows[0][0].ToString();
            DataSet cash = db.Select("select SUM(Bill) from sale where Date ='" + dateTimePicker1.Value + "' ");
            txt_tot_cash.Text = cash.Tables["tbl1"].Rows[0][0].ToString();
        }

        private void btnsrc3_Click(object sender, EventArgs e)
        {
            DataSet ds = db.Select("Select Sale_ID,Item_code,Item_Name,Quantity,Price,BILL,Date from sale where MONTH(date)='" + combo_month.Text + "' and Year(date)='" + combo_year.Text + "' ");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
            DataSet cou = db.Select("select count(sale_ID) from sale where MONTH(date)='" + combo_month.Text + "' and Year(date)='" + combo_year.Text + "'");
            txt_totl_sales.Text = cou.Tables["tbl1"].Rows[0][0].ToString();
            DataSet cash = db.Select("select SUM(Bill) from sale where MONTH(date)='" + combo_month.Text + "' and Year(date)='" + combo_year.Text + "' ");
            txt_tot_cash.Text = cash.Tables["tbl1"].Rows[0][0].ToString();
            //month_ser();
            //DataSet ds = db.Select("Select Sale_ID,Item_code,Item_Name,Quantity,Price,BILL,Date from sale where Date='" + combo_month.Text+ "' ");
            //dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
            //DataSet cou = db.Select("select count(sale_ID) from sale where Date='" + combo_month.Text + "'");
            //txt_totl_sales.Text = cou.Tables["tbl1"].Rows[0][0].ToString();
            //DataSet cash = db.Select("select SUM(Bill) from sale where Date='" + combo_month.Text + "' ");
            //txt_tot_cash.Text = cash.Tables["tbl1"].Rows[0][0].ToString();
        }

        private void combo_year_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        }

        

    }

