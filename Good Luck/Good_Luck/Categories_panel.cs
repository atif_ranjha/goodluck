﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using DAL;

namespace Bakery_Management_System
{
    public partial class Categories_panel : UserControl
    {
        public Categories_panel()
        {
            InitializeComponent();
        }
        DB_Connecctions db = new DB_Connecctions();
        string con;
        
        //************** Refre ///////
        public void refre()
        {
            DataSet ds = db.Select("select ID,CategoryName,CategoryDescription,CategoryPicture from Categories where status=1");
                dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;

               // DataGridViewImageColumn imageColumn = new DataGridViewImageColumn();
               // Image img;
               //// DataTable dt;
               // int i = 0;
               // imageColumn.HeaderText = "FileName";
               // imageColumn.Name = "FilePath";
               // dataGridView1.Columns.Insert(3, imageColumn);

               // foreach (DataGridViewRow dr in dataGridView1.Rows)
               // {
               //     img = Image.FromFile(Application.StartupPath + @"\" + dr.Cells[5].Value.ToString() + @"\" + dr.Cells[4].Value.ToString());
               //     dataGridView1.Rows[0].Cells[5].Value = img;
               //     i++;
               // }
                
        }
        
        //********** Get ID
        // *****+++++++++++++++ TO GET ITEM CODE ++++++++++++*******//
        public void get_id()
        {
            DataSet ds = db.Select("select isnull(max(cast(ID as int)),0)+1 from Categories");
            txt_ID.Text = ds.Tables["tbl1"].Rows[0][0].ToString();
        }
        //********** Save Catogryyyy Code///
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                con = ConnectionString.ConString;
                SqlConnection conn = new SqlConnection(con);
                conn.Open();
                //SqlCommand cmd = new SqlCommand("insert into Categories(ID,CategoryName,Categorydescription,CategoryPicture,status) values('"+txt_ID.Text+"','" + txt_CategoryName.Text + "','" + txt_CategoryDescription.Text + "',@CategoryPicture,1)", conn);
                SqlCommand cmd = new SqlCommand("insert into Categories(ID,CategoryName,Categorydescription,status,FileName,FilePath,CategoryPicture) values('" + txt_ID.Text + "','" + txt_CategoryName.Text + "','" + txt_CategoryDescription.Text + "',1,'" + Path.GetFileName(this.openFileDialog1.FileName) + "','" + @"\images\" + textBox1.Text + "',@CategoryPicture)", conn);

                MemoryStream ms = new MemoryStream();
                /*saving the image in raw format from picture box*/
                CategoryPictureBox.Image.Save(ms, CategoryPictureBox.Image.RawFormat);
                /*Array of Binary numbers that have been converted*/
                byte[] CategoryPicture = ms.GetBuffer();
                cmd.Parameters.AddWithValue("@CategoryPicture", CategoryPicture);
                cmd.ExecuteNonQuery();
                conn.Close();
                refre();
                get_id();
                txt_CategoryName.Clear();
                txt_CategoryDescription.Clear();
                CategoryPictureBox.Image = null;
                MessageBox.Show("New Category Added.","Category section",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                
              
            }
        }
        //************** UPLOAD PICTURE /////
        private void UploadPictureButton_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.ShowDialog();
            //MessageBox.Show("Save In Folder");
            //OpenFileDialog ofd = new OpenFileDialog();

            //ofd.Title = "Select Image file..";
            //ofd.DefaultExt = ".jpg";
            //ofd.Filter = "Media Files|*.jpg;*.png;*.gif;*.bmp;*.jpeg|All Files|*.*";

            //DialogResult result = ofd.ShowDialog();
            //if (result == DialogResult.OK)
            //{
            //    /*if picture selected then load in the picture box*/
            //    CategoryPictureBox.Load(ofd.FileName);
            //}
        }

        private void Categories_panel_Load(object sender, EventArgs e)
        {
            refre();
            get_id();
        }
        //*********** Cell Click to load on Form
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                    txt_ID.Text = row.Cells[0].Value.ToString();
                    txt_CategoryName.Text = row.Cells[1].Value.ToString();
                    txt_CategoryDescription.Text = row.Cells[2].Value.ToString();
                    byte[] pict = (byte[])(row.Cells[3].Value);
                    MemoryStream ms = new MemoryStream(pict);
                    CategoryPictureBox.Image = Image.FromStream(ms);
                }
            }
            catch (Exception)
            {
                
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                con = ConnectionString.ConString;
                SqlConnection conn = new SqlConnection(con);
                conn.Open();
                SqlCommand cmd = new SqlCommand("Update Categories Set CategoryName='" + txt_CategoryName.Text + "',Categorydescription='" + txt_CategoryDescription.Text + "' Where ID='" + txt_ID.Text + "'", conn);
                MemoryStream ms = new MemoryStream();
                /*saving the image in raw format from picture box*/
                ////CategoryPictureBox.Image.Save(ms, CategoryPictureBox.Image.RawFormat);
                /*Array of Binary numbers that have been converted*/
              //byte[] CategoryPicture = ms.GetBuffer();
               // cmd.Parameters.AddWithValue("@CategoryPicture", CategoryPicture);
                cmd.ExecuteNonQuery();
                conn.Close();
                refre();
                get_id();
                txt_CategoryName.Clear();
                txt_CategoryDescription.Clear();
                MessageBox.Show("Category Updated.", "Category section", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            db.ExecuteQuery("Update Categories Set Status=0 where ID='"+txt_ID.Text+"'");
            get_id();
            refre();
            txt_CategoryName.Clear();
            txt_CategoryDescription.Clear();
            MessageBox.Show("Category Deleted.", "Category section", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            get_id();
            refre();
            txt_CategoryName.Clear();
            txt_CategoryDescription.Clear();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txt_ID_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txt_CategoryName_TextChanged(object sender, EventArgs e)
        {

        }

        private void CategoryPictureBox_Click(object sender, EventArgs e)
        {

        }

        private void txt_CategoryDescription_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string folderPath = (@"\images\" + textBox1.Text);
            //  string path = @"\Debug\";
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + folderPath))
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + folderPath);
            }
            //string sqlStatment = "INSERT INTO Categories VALUES(@CategoryName,@CategoryDescription,@Status,@FileName,@FilePath)";
            //con = ConnectionString.ConString;
            //using (SqlConnection con1 = new SqlConnection(con))
            //{
            //    using (SqlCommand cmd = new SqlCommand(sqlStatment, con1))
            //    {
            //        con1.Open();

            //        cmd.Parameters.AddWithValue("@CategoryName", txt_CategoryName.Text);
            //        cmd.Parameters.AddWithValue("@CategoryDescription", txt_CategoryDescription.Text);
            //        //cmd.Parameters.AddWithValue("@CategoryPicture", CategoryPictureBox.Image);
            //        cmd.Parameters.AddWithValue("@Status", 1);
            //        cmd.Parameters.AddWithValue("@FileName", Path.GetFileName(this.openFileDialog1.FileName));
            //        cmd.Parameters.AddWithValue("@FilePath", folderPath + @"\");
            //        cmd.ExecuteNonQuery();
            //        con1.Close();
            //    }
            //}

            //Save the file in folder
            CategoryPictureBox.Image = new Bitmap(openFileDialog1.FileName);
            string fileLocation = openFileDialog1.FileName;
            // string startupPath = System.IO.Directory.GetParent(@"C:\bin\Debug\Images\" + textBox2.Text, Path.GetFileName(fileLocation)), true");.FullName;
            // File.Copy(fileLocation,  Path.Combine(@"\bin\Debug\" + textBox2.Text);
            File.Copy(fileLocation, Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"\images\" + textBox1.Text, Path.GetFileName(fileLocation)), true);
        }
    }
}
