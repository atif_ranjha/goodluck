﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bakery_Management_System
{
    public partial class Mini_Stock : UserControl
    {
        public Mini_Stock()
        {
            InitializeComponent();
        }
        // *****+++++++++++++++ DATABASE CONNECTION CLASS GLOBAL ++++++++++++*******//
        DB_Connecctions db = new DB_Connecctions();
        // *****+++++++++++++++ TO REFRESH GRID VIEW ++++++++++++*******//
        public void refre()
        {
            DataSet ds = db.Select("select * from Items");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
        }
        // *****+++++++++++++++ TO COUNT ITEMS IN STOCK Form ++++++++++++*******//
        public void count_item()
        {
            DataSet ds = db.Select("select count(item_code) from Items");
            lbl_count.Text = ds.Tables["tbl1"].Rows[0][0].ToString();
        }
        //**+++++ TO SHOW LOW STOCK WARNING //++++++*****
        public void low_stock()
        {
            DataSet ds = db.Select("select * from items where Item_Stock <10 ");
            if (ds != null && ds.Tables["tbl1"].Rows.Count > 0)
            {
                label10.ForeColor = System.Drawing.Color.Red;
                label10.Text = "Warning! Items are at low of stock";
            }
            else
            {
                label10.Text = "";
            }

        }
        //***** TO SHOW LOW STOCK ITEMS IN  GRID VIEW ///******
        public void low_item()
        {
            DataSet ds = db.Select("select * from items where Item_Stock <10 ");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
        }
        //**** +++ SEARCH TEXT BOX CODE ///***++++++
        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            DataSet ds = db.Select("select * from Items where item_code like '%" + txt_search.Text + "%' or item_name like '%" + txt_search.Text + "%'");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
        }
        //**** +++ REFRESH BUTTON CODE ///***++++++
        private void btn_clear_Click(object sender, EventArgs e)
        {
            refre();
        }
        //**** ON LOAD EVENTES */**++++++++++++++
        private void Mini_Stock_Load(object sender, EventArgs e)
        {
           
            count_item();
            low_stock();
            if (label10.Text == "Warning! Items are at low of stock")
            {
                low_item();
            }
            else
            {
                refre();// TO Load Gridview on Form Load
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            low_item();
        }

    }
}
