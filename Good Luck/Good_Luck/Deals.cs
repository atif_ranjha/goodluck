﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using DAL;

namespace Bakery_Management_System
{
    public partial class Deals : UserControl
    {
        public Deals()
        {
            InitializeComponent();
        }
        // *****+++++++++++++++ DATABASE CONNECTION CLASS GLOBAL ++++++++++++*******//
        DB_Connecctions db = new DB_Connecctions();
        string con;
        // *****+++++++++++++++ TO REFRESH GRID VIEW ++++++++++++*******//
        public void refre()
        {
            DataSet ds = db.Select("select Deal_ID,Deal_Name,Deal_Detail,Price,Type,CatogryID,Deal_Image from Deals where Status=1 order by CatogryID");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
        }
        // *****+++++++++++++++ TO GET ITEM CODE ++++++++++++*******//
        public void get_id()
        {
            DataSet ds = db.Select("select Deal_ID from Deals");
            if (ds != null && ds.Tables["tbl1"].Rows.Count > 0)
            {
                int a = ds.Tables["tbl1"].Rows.Count;
                txt_item_Code.Text = (Convert.ToInt32(ds.Tables["tbl1"].Rows[a - 1][0]) + 1).ToString();
            }
            else
            {
                txt_item_Code.Text = "1";
            }
        }
        // *****+++++++++++++++ TO COUNT ITEMS IN STOCK Form ++++++++++++*******//
        public void count_item()
        {
            DataSet ds = db.Select("select count(Deal_ID) from deals where status=1");
            lbl_count.Text = ds.Tables["tbl1"].Rows[0][0].ToString();
        }
        //********* Get Categories Detail //////
        public void categ()
        {
            DataSet dba = db.Select("select * from Categories where Status=1");
            if (dba != null && dba.Tables["tbl1"].Rows.Count > 0)
            {
                combo_category.DataSource = dba.Tables["tbl1"].DefaultView;
                combo_category.DisplayMember = "CategoryName";
                combo_category.ValueMember = "ID";
            }
        }
        // *****+++++++++++++++ TO Clear Form ++++++++++++*******//
        public void clear()
        {
            txt_discription.Clear();
            txt_item_name.Clear();
            txt_price.Clear();
            CategoryPictureBox.Image = null;
        }
        private void btn_clear_Click(object sender, EventArgs e)
        {
            clear();
            get_id();
            btn_save.Enabled = true;
            btn_delete.Enabled = false;
            btn_update.Enabled = false;
        }
        //********* LOAD EVENT ////////
        private void Deals_Load(object sender, EventArgs e)
        {
            categ();
            refre();// TO Load Gridview on Form Load
            get_id();// TO GET ITEM CODE on FORM LOAD
            count_item();//TO COUNT ITEMS IN STOCK
            //Enable and Disable button on form Load
            btn_save.Enabled = true;
            btn_update.Enabled = false;
            btn_delete.Enabled = false;
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (txt_item_name.Text == "" || txt_discription.Text == "" || txt_price.Text == "")
            {
                MessageBox.Show("Please! Fill all the Fields.", "Deals", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    con = ConnectionString.ConString;
                    SqlConnection conn = new SqlConnection(con);
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("Insert into Deals(Deal_ID,Deal_Name,Deal_Detail,Price,Type,CatogryID,Deal_Image,Status,Recipe,FileName,FilePath) values('" + txt_item_Code.Text + "','" + txt_item_name.Text + "','" + txt_discription.Text + "','" + txt_price.Text + "','" + combo_type.Text + "','" + combo_category.SelectedValue.ToString() + "',@Pic,1,0,'" + Path.GetFileName(this.openFileDialog1.FileName) + "','" + @"\images\" + txt_discription.Text + "')", conn);
                    MemoryStream ms = new MemoryStream();
                    /*saving the image in raw format from picture box*/
                    CategoryPictureBox.Image.Save(ms, CategoryPictureBox.Image.RawFormat);
                    /*Array of Binary numbers that have been converted*/
                    byte[] CategoryPicture = ms.GetBuffer();
                    cmd.Parameters.AddWithValue("@Pic", CategoryPicture);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    clear();
                    get_id();
                    refre();
                    count_item();
                    txt_item_name.Focus();
                    MessageBox.Show("New Deal Saved Succefully.", "Deals", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                catch
                {

                }
            }
        }
        //**** Update Deal //////////
        private void btn_update_Click(object sender, EventArgs e)
        {
            //try
            //{

            con = ConnectionString.ConString;
            SqlConnection conn = new SqlConnection(con);
            conn.Open();
            SqlCommand cmd = new SqlCommand("Update Deals set Deal_Name='" + txt_item_name.Text + "',Deal_Detail='" + txt_discription.Text + "',Price='" + txt_price.Text + "',Type='" + combo_type.Text + "',CatogryID='" + combo_category.SelectedValue.ToString() + "' where Deal_ID='" + txt_item_Code.Text + "'", conn);
           // MemoryStream ms = new MemoryStream();
            /*saving the image in raw format from picture box*/
           // CategoryPictureBox.Image.Save(ms, CategoryPictureBox.Image.RawFormat);
            /*Array of Binary numbers that have been converted*/
           //// byte[] CategoryPicture = ms.GetBuffer();
           // cmd.Parameters.AddWithValue("@Pic", CategoryPicture);
            cmd.ExecuteNonQuery();
            conn.Close();
            clear();
            get_id();
            refre();
            count_item();
            txt_item_name.Focus();
            MessageBox.Show("Deal Updated Succefully.", "Deals", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
            //catch (Exception)
            //{

            //}
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            try
            {

                db.ExecuteQuery("update Deals set Status=0 where Deal_ID='" + txt_item_Code.Text + "'");
                MessageBox.Show("Deal Deleted Succefully.", "Deals Section", MessageBoxButtons.OK, MessageBoxIcon.Information);
                clear();
                get_id();
                refre();
                count_item();
                btn_save.Enabled = true;
                btn_delete.Enabled = false;
                btn_update.Enabled = false;
                txt_item_name.Focus();
            }
            catch (Exception)
            {

            }
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            DataSet ds = db.Select("select Deal_ID,Deal_Name,Deal_Detail,Price,Type,CatogryID from Deals where (Deal_ID like '%" + txt_search.Text + "%' and Status=1) or (Deal_name like '%" + txt_search.Text + "%' and Status=1)");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                    txt_item_Code.Text = row.Cells[0].Value.ToString();
                    txt_item_name.Text = row.Cells[1].Value.ToString();
                    txt_discription.Text = row.Cells[2].Value.ToString();
                    combo_type.Text = row.Cells[4].Value.ToString();
                    txt_price.Text = row.Cells[3].Value.ToString();
                    combo_category.Text = row.Cells[5].Value.ToString();
                    byte[] pict = (byte[])(row.Cells[6].Value);
                    MemoryStream ms = new MemoryStream(pict);
                    CategoryPictureBox.Image = Image.FromStream(ms);
                    btn_save.Enabled = false;
                    btn_delete.Enabled = true;
                    btn_update.Enabled = true;
                }
                //btn_save.Enabled = false;
                //btn_delete.Enabled = true;
                //btn_update.Enabled = true;
            }
            catch (Exception)
            {

            }
        }
        //*** ************ UPLOAD PICTURE /////**********
        private void UploadPictureButton_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.ShowDialog();
            //OpenFileDialog ofd = new OpenFileDialog();

            //ofd.Title = "Select Image file..";
            //ofd.DefaultExt = ".jpg";
            //ofd.Filter = "Media Files|*.jpg;*.png;*.gif;*.bmp;*.jpeg|All Files|*.*";

            //DialogResult result = ofd.ShowDialog();
            //if (result == DialogResult.OK)
            //{
            //    /*if picture selected then load in the picture box*/
            //    CategoryPictureBox.Load(ofd.FileName);
            //}
        }

        private void combo_category_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CategoryPictureBox_Click(object sender, EventArgs e)
        {

        }

        private void combo_type_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string folderPath = (@"\images\" + txt_discription.Text);
            //  string path = @"\Debug\";
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + folderPath))
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + folderPath);
            }
            //string sqlStatment = "INSERT INTO Categories VALUES(@CategoryName,@CategoryDescription,@Status,@FileName,@FilePath)";
            //con = ConnectionString.ConString;
            //using (SqlConnection con1 = new SqlConnection(con))
            //{
            //    using (SqlCommand cmd = new SqlCommand(sqlStatment, con1))
            //    {
            //        con1.Open();

            //        cmd.Parameters.AddWithValue("@CategoryName", txt_CategoryName.Text);
            //        cmd.Parameters.AddWithValue("@CategoryDescription", txt_CategoryDescription.Text);
            //        //cmd.Parameters.AddWithValue("@CategoryPicture", CategoryPictureBox.Image);
            //        cmd.Parameters.AddWithValue("@Status", 1);
            //        cmd.Parameters.AddWithValue("@FileName", Path.GetFileName(this.openFileDialog1.FileName));
            //        cmd.Parameters.AddWithValue("@FilePath", folderPath + @"\");
            //        cmd.ExecuteNonQuery();
            //        con1.Close();
            //    }
            //}

            //Save the file in folder
            CategoryPictureBox.Image = new Bitmap(openFileDialog1.FileName);
            string fileLocation = openFileDialog1.FileName;
            // string startupPath = System.IO.Directory.GetParent(@"C:\bin\Debug\Images\" + textBox2.Text, Path.GetFileName(fileLocation)), true");.FullName;
            // File.Copy(fileLocation,  Path.Combine(@"\bin\Debug\" + textBox2.Text);
            File.Copy(fileLocation, Path.Combine(AppDomain.CurrentDomain.BaseDirectory + @"\images\" + txt_discription.Text, Path.GetFileName(fileLocation)), true);
        }
    }
}
