﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bakery_Management_System
{
    public partial class main : Form
    {
        public main()
        {
            InitializeComponent();
        }
        //*** GLOBAL CLASS FOR CONNECTIONS /////
        DB_Connecctions db = new DB_Connecctions();
        private void exitToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void main_Load(object sender, EventArgs e)
        {
            this.main_panel.Controls.Clear();
            Point_Sale pos = new Point_Sale();
            pos.Dock = DockStyle.Fill;
            this.main_panel.Controls.Add(pos); 
                       
        }

        private void itemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.main_panel.Controls.Clear();
            Items ite = new Items();
            ite.Dock = DockStyle.Fill;
            this.main_panel.Controls.Add(ite);
        }

        private void saleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.main_panel.Controls.Clear();
            Point_Sale pos = new Point_Sale();
            pos.Dock = DockStyle.Fill;
            this.main_panel.Controls.Add(pos); 
        }

        private void stockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //this.main_panel.Controls.Clear();
            //Stock stk = new Stock();
            //stk.Dock = DockStyle.Fill;
            //this.main_panel.Controls.Add(stk);
        }

        private void accountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.main_panel.Controls.Clear();
            Admin adm = new Admin();
            adm.Dock = DockStyle.Fill;
            this.main_panel.Controls.Add(adm );
        }

        private void changeUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            Login lg = new Login();
            lg.Show();
            this.Hide();
        }

        private void main_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dr = MessageBox.Show("Do you want to create backup?","BackUp",MessageBoxButtons.YesNo,MessageBoxIcon.Question);
            if (dr==DialogResult.Yes)
            {

                    string f;
                    saveFileDialog1.Filter = "SQL Backup File (*.bak)|*.bak|All files (*.*)|*.*";
                    saveFileDialog1.FileName = "QFC "+ System.DateTime.Today.ToShortDateString() +" Backup"+ ".bak";
                    if (saveFileDialog1.ShowDialog()==DialogResult.OK)
                    {
                        f = saveFileDialog1.FileName;
                        db.ExecuteQuery("backup database QFC to disk= '" + f + "'");
                        MessageBox.Show("Saved ");    
                    }
                    
                    
            }
          //  Application.Exit();
        }

        private void logManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.main_panel.Controls.Clear();
            Statistics st = new Statistics();
            st.Dock = DockStyle.Fill;
            this.main_panel.Controls.Add(st);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About ab = new About();
            ab.Show();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Application.StartupPath + "\\Bakery.chm");
        }

        private void dealsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.main_panel.Controls.Clear();
            Deals st = new Deals();
            st.Dock = DockStyle.Fill;
            this.main_panel.Controls.Add(st);
        }

        private void receipeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.main_panel.Controls.Clear();
            POS st = new POS();
            st.ShowDialog();
           

           // st.Dock = DockStyle.Fill;
            //this.main_panel.Controls.Add(st);
        }

        private void expencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.main_panel.Controls.Clear();
            Expences st = new Expences();
            st.Dock = DockStyle.Fill;
            this.main_panel.Controls.Add(st);
        }

        private void catogryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.main_panel.Controls.Clear();
            Categories_panel st = new Categories_panel();
            st.Dock = DockStyle.Fill;
            this.main_panel.Controls.Add(st);
        }

        private void employeeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.main_panel.Controls.Clear();
            Employees emp = new Employees();
            emp.Dock = DockStyle.Fill;
            this.main_panel.Controls.Add(emp);



        }

        private void departmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.main_panel.Controls.Clear();
            DEPARTMENT dp = new DEPARTMENT();
            dp.Dock = DockStyle.Fill;
            this.main_panel.Controls.Add(dp);

        }

        private void designationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.main_panel.Controls.Clear();
            designation ds = new designation();
            ds.Dock = DockStyle.Fill;
            this.main_panel.Controls.Add(ds);

        }
    }
}
