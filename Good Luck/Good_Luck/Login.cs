﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace Bakery_Management_System
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            string superadd;
            DB_Connecctions con = new DB_Connecctions();
            DataSet log_in = con.Select("select * from Admin where user_Name='" + txt_username.Text + "' and password='" + txt_password.Text + "'");
            if (log_in == null || log_in.Tables["tbl1"].Rows.Count <= 0)
            {
                MessageBox.Show("Username or Password is incorrect.", "Log In", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DataSet super_admin = con.Select("select Status from admin where user_Name='" + txt_username.Text + "' and password='" + txt_password.Text + "'");
                superadd = super_admin.Tables[0].Rows[0]["Status"].ToString();
                if (superadd.ToString() == "Admin")
                {
                    main mn = new main();
                    mn.Show();
                    this.Hide();
                }
                else
                {
                    User use = new User();
                    use.Show();
                    this.Hide();
                }
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            //label3.Text = Convert.ToString(System.tin,"Long Time");
            DialogResult dr = MessageBox.Show("Are you sure you want to exit?", "Log In", MessageBoxButtons.YesNo,MessageBoxIcon.Question);
            if(dr==DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {

            }
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.CheckState == CheckState.Checked)
            {
                txt_password.UseSystemPasswordChar = false;
            }
            else
            {
                txt_password.UseSystemPasswordChar = true;
            }
        }
    }
}
