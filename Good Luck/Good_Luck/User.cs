﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace Bakery_Management_System
{
    public partial class User : Form
    {
        public User()
        {
            InitializeComponent();
        }
        //***** TO LOAD SALE PANNELON FORM LOAD //+++++++++
        private void User_Load(object sender, EventArgs e)
        {
            
            this.main_panel.Controls.Clear();
            Point_Sale pos = new Point_Sale();
            pos.Dock = DockStyle.Fill;
            this.main_panel.Controls.Add(pos); 
        }
        //***** TO Exit Application //+++++++++
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //***** TO Exit Application //+++++++++
        private void User_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
        //**** POS SALE CLICK //**************
        private void pointOfSaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.main_panel.Controls.Clear();
            Point_Sale pos = new Point_Sale();
            pos.Dock = DockStyle.Fill;
            this.main_panel.Controls.Add(pos);
        }
        //**** ITEMS CLICK //**************
        private void itemsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.main_panel.Controls.Clear();
            Mini_Stock pos = new Mini_Stock();
            pos.Dock = DockStyle.Fill;
            this.main_panel.Controls.Add(pos);
        }
        //**** CHANGE USER CLICK CLICK //**************
        private void changeUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Login lg = new Login();
            lg.Show();
            this.Hide();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Application.StartupPath + "\\Bakery.chm");
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About ab = new About();
            ab.Show();
        }
    }
}
