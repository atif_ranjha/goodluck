﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using DAL;

namespace Bakery_Management_System
{
    public partial class POS : Form
    {
        public POS()
        {
            InitializeComponent();
        }
        bool checktable;
        bool checkpayment;
        bool checkzero;


        int amount = 0;
        int RowIndex = 0;
        string query;
        public static string printer;
        // SqlConnection con= new SqlConnection(@"Data Source=DELL-INSPIRON;Initial Catalog=Bakery;Integrated Security=True");
        //****+++++++++++ DATABASE CONNECTION **+++++++++
        DB_Connecctions db = new DB_Connecctions();
        //****+++++++ LIST VIEW ON LOAD  ******+++++++++++
        //****+++++++++++ TO GET BATCH ID **+++++++++
        public void get_id()
        {
            DataSet ds = db.Select("select isnull(max(cast(Batch_ID as int)),0)+1 from Sale");
            textBox1.Text = ds.Tables["tbl1"].Rows[0][0].ToString();
        }
        //**** LOW STOCK ITEMS SHOW //****
        public void low_stock()
        {
            DataSet ds = db.Select("select * from items where Item_Stock<=Item_Price");
            if (ds != null && ds.Tables["tbl1"].Rows.Count > 0)
            {
               // label16.ForeColor = System.Drawing.Color.Red;
               // label16.Text = "Warning! Items are at low of stock";
            }
        }
        //***++++++++ TO GETPRINTER NAME ///***+++++
        public void get_name()
        {
            DataSet name = db.Select("select name from printer where printer_ID='1'");
            if (name != null && name.Tables["tbl1"].Rows.Count > 0)
            {
                label18.Text = name.Tables["tbl1"].Rows[0]["Name"].ToString();

            }
        }
        public void list_box3()
        {
            DataSet ds = db.Select("select distinct(Batch_ID) from Sale where Payment=0 and Table_No='Delivery' and status!='K'");


            if (ds.Tables["tbl1"].Rows.Count > 0)
            {
                this.listBox3.DataSource = ds.Tables["tbl1"].DefaultView;
                this.listBox3.DisplayMember = "batch_ID";
            }
            clear();
            // ProductsGridView.Rows.Clear();

        }
        public void list_Box()
        {
            DataSet ds = db.Select("select distinct(batch_ID) from Sale where Payment=0 and Table_No='Advance'");

            //DataSet ds = db.Select("select distinct(batch_ID) from Adv_order   where status='Unpaid'");
            if (ds.Tables["tbl1"].Rows.Count > 0)
            {
                this.listBox1.DataSource = ds.Tables["tbl1"].DefaultView;
                this.listBox1.DisplayMember = "batch_ID";
            }
            clear();
            // ProductsGridView.Rows.Clear();

        }
        public void list_Box2()
        {

            DataSet ds = db.Select("select distinct(Table_No) from Sale where Payment=0 and Table_No !='Advance' and Table_No !='Take Away' and Table_No !='Delivery' ");
            if (ds.Tables["tbl1"].Rows.Count > 0)
            {
                this.listBox2.DataSource = ds.Tables["tbl1"].DefaultView;
                this.listBox2.DisplayMember = "Table_No";
            }
            checktable = true;
            checkpayment = true;
            clear();
            // ProductsGridView.Rows.Clear();


        }
        //////////////////////////////////////////////////////////////////////////////
        ////////////*****************************
        //************      Retrive Products details ///////
        // ALL CATEGORY GETFUNCTION
        
           
        private void kitchen_CR(string seacrch)
        {
            DB_Connecctions db = new DB_Connecctions();
            string conreg = ConnectionString.ConString;
            SqlConnection con = new SqlConnection(conreg);
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = seacrch;
            cmd.ExecuteNonQuery();
            Bakery_Sale_Receipt_DataSet ds = new Bakery_Sale_Receipt_DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds, "Sale");
            kitchen_recipet myreport = new kitchen_recipet();
            myreport.SetDataSource(ds);
            myreport.PrintOptions.PrinterName = label18.Text;
            myreport.PrintToPrinter(1, false, 0, 0);
            //crystalReportViewer1.ReportSource = myreport;
            con.Close();
        }

        private void CR_Form(string seacrch)
        {
            DB_Connecctions db = new DB_Connecctions();
            string conreg = ConnectionString.ConString;
            SqlConnection con = new SqlConnection(conreg);
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = seacrch;
            cmd.ExecuteNonQuery();
            Bakery_Sale_Receipt_DataSet ds = new Bakery_Sale_Receipt_DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds, "Sale");
            Rpt_Receipt myreport = new Rpt_Receipt();
            myreport.SetDataSource(ds);
            myreport.PrintOptions.PrinterName = label18.Text;
            myreport.PrintToPrinter(1, true, 0, 0);
            //crystalReportViewer1.ReportSource = myreport;
            con.Close();
        }



        //*** PROCESS OR SAVE SALE FUNCTION
        public void process_save()
        {
            try
            {
                if (combo_table.Text == "")
                {
                    MessageBox.Show("Please Select Table or Deal.", "POS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (ProductsGridView.Rows.Count > 0)
                    {
                        for (int i = 0; i < ProductsGridView.Rows.Count - 1; i++)
                        {

                            db.ExecuteQuery("insert into sale (Batch_ID, Name, Phone_No ,Address,item_code,item_name,quantity,price,bill,Food_Type,total_amount,Paid_Amount,Discount,date,Status,Table_No,Payment) values('" + textBox1.Text + "','" + txt_custname.Text + "','" + textBox4.Text + "','" + textAddress.Text + "','" + ProductsGridView.Rows[i].Cells[0].Value + "','" + ProductsGridView.Rows[i].Cells[1].Value + "','" + ProductsGridView.Rows[i].Cells[3].Value + "','" + ProductsGridView.Rows[i].Cells[2].Value + "','" + ProductsGridView.Rows[i].Cells[4].Value + "','" + ProductsGridView.Rows[i].Cells[5].Value + "','" + txt_grand_total.Text + "','" + txt_grand_total.Text + "',0,'" + dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss") + "','S','" + combo_table.Text + "',1)");
                            //DataSet ds = db.Select("select Ingredient,Quantity from Recipes Where Deal_ID='" + ProductsGridView.Rows[i].Cells[0].Value + "'");
                            //dataGridView2.DataSource = ds.Tables["tbl1"].DefaultView;
                            //for (int l = 0; l < dataGridView2.Rows.Count - 1; l++)
                            //{
                            //    int d = Convert.ToInt32(dataGridView2.Rows[i].Cells[1].Value);
                            //    int j = Convert.ToInt32(ProductsGridView.Rows[i].Cells[2].Value);
                            //    db.ExecuteQuery("Update Items set item_Stock=item_Stock - " + d + " * " + j + " where Item_Name='" + dataGridView2.Rows[i].Cells[0].Value + "'");
                            //}

                        }
                        //MessageBox.Show("New Sale Added.", "POS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //kitchen_CR("select * from sale where batch_ID='" + textBox1.Text + "' and Status='S'");
                        //db.ExecuteQuery("Update sale set Status='K' where batch_id='" + textBox1.Text + "' and status='S'");
                        CR_Form("select * from Sale where Batch_ID=" + textBox1.Text + "");
                        list_Box();
                        get_id();//** to get batch ID
                        txt_grand_total.Text = "0";
                        low_stock();// LOW STOCK LABLE

                    }//***  End If to Check list View is > 0
                }

            }//**** End Try
            catch (Exception ex)
            {

            }///***** End catch   
        }// END FUNCTION

        //***+++ BUTTON PROCESS CODE ///**++++++++

        private void Delivery_radio_CheckedChanged(object sender, EventArgs e)
        {
            combo_table.Enabled = false;
            if (Delivery_radio.Checked == true)
            {
                
                combo_table.Text = "Delivery";
                SaveBtn.Hide();
                SaveAdvance.Hide();
                Delivery_btn.Show();
                button1.Show();
                btn_process.Hide();
                button4.Hide();
                clear();

            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SaveAdvance.Hide();
            //adv_search("select Item_code,Item_Name,Quantity,Price,Bill,Total_amount,batch_ID from Adv_order where Batch_id='" + listBox1.Text + "' and status='Unpaid'");
            if (listBox1.Text != "System.Data.DataRowView" && !string.IsNullOrEmpty(listBox1.Text))
            {
                DataSet ds = db.Select("Select * from sale Where Batch_ID='" + listBox1.Text + "' and payment=0 ");
                if (ds.Tables["tbl1"].Rows.Count > 0)
                {
                    combo_table.Text = ds.Tables["tbl1"].Rows[0]["Table_No"].ToString();
                    textBox1.Text = ds.Tables["tbl1"].Rows[0]["Batch_ID"].ToString();
                    txt_custname.Text = ds.Tables["tbl1"].Rows[0]["Name"].ToString();
                    textBox4.Text = ds.Tables["tbl1"].Rows[0]["Phone_No"].ToString();
                    txt_grand_total.Text = ds.Tables["tbl1"].Rows[0]["Total_Amount"].ToString();
                    ProductsGridView.Rows.Clear();
                    //clear();
                    for (int i = 0; i < ds.Tables["tbl1"].Rows.Count; i++)
                    {
                        {
                            //string item_name = ProductsGridView.Rows[i].Cells[1].Value.ToString();
                            string ID = ds.Tables["tbl1"].Rows[i]["Item_Code"].ToString();
                            string item_name = ds.Tables["tbl1"].Rows[i]["Item_Name"].ToString();
                            string price = ds.Tables["tbl1"].Rows[i]["Price"].ToString();
                            string quantity = ds.Tables["tbl1"].Rows[i]["Quantity"].ToString();
                            string Product_Total = ds.Tables["tbl1"].Rows[i]["Bill"].ToString();
                            string type = ds.Tables["tbl1"].Rows[i]["Food_Type"].ToString();
                            ProductsGridView.Rows.Add(ID, item_name, price, quantity, Product_Total, type);

                        }
                    }

                }
            }
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            list_Box();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            get_id();
            OrderChange Order = new OrderChange();
            Order.ShowDialog();
        }

        private void listBox2_DoubleClick(object sender, EventArgs e)
        {
            list_Box2();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            SaveBtn.Hide();
            if (listBox2.Text != "System.Data.DataRowView" && !string.IsNullOrEmpty(listBox2.Text))
            {
                DataSet ds = db.Select("Select * from sale Where Table_no='" + listBox2.Text + "' and Payment=0");
                if (ds.Tables["tbl1"].Rows.Count > 0)
                {
                    checktable = false;
                    combo_table.Text = ds.Tables["tbl1"].Rows[0]["Table_No"].ToString();
                    textBox1.Text = ds.Tables["tbl1"].Rows[0]["Batch_ID"].ToString();
                    txt_custname.Text = ds.Tables["tbl1"].Rows[0]["Name"].ToString();
                    textBox4.Text = ds.Tables["tbl1"].Rows[0]["Phone_No"].ToString();
                    txt_grand_total.Text = ds.Tables["tbl1"].Rows[0]["Total_Amount"].ToString();
                    ProductsGridView.Rows.Clear();
                    //clear();
                    for (int i = 0; i < ds.Tables["tbl1"].Rows.Count; i++)
                    {
                        //string item_name = ProductsGridView.Rows[i].Cells[1].Value.ToString();
                        string ID = ds.Tables["tbl1"].Rows[i]["Item_Code"].ToString();
                        string item_name = ds.Tables["tbl1"].Rows[i]["Item_Name"].ToString();
                        string price = ds.Tables["tbl1"].Rows[i]["Price"].ToString();
                        string quantity = ds.Tables["tbl1"].Rows[i]["Quantity"].ToString();
                        string Product_Total = ds.Tables["tbl1"].Rows[i]["Bill"].ToString();
                        string type = ds.Tables["tbl1"].Rows[i]["Food_Type"].ToString();
                        ProductsGridView.Rows.Add(ID, item_name, price, quantity, Product_Total, type);
                    }

                }
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            //get_id();
            Payment pc = new Payment(ProductsGridView, textBox1.Text, txt_grand_total.Text, txt_custname.Text, textBox4.Text,combo_table.Text,textAddress.Text);
            pc.ShowDialog();
            get_id();
            txt_custname.Clear();
            txt_grand_total.Clear();
            textBox4.Clear();
        }

        private void ProductsGridView_Click(object sender, EventArgs e)
        {

        }

        private void ProductsGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    if (ProductsGridView.Columns[e.ColumnIndex].Name == "DeleteColumn")
                    {

                        decimal DeletedProductTotal = Convert.ToDecimal(ProductsGridView.Rows[e.RowIndex].Cells["TotalPriceColumn"].Value);

                        decimal CurrentTotalBill = Convert.ToDecimal(txt_grand_total.Text);

                        CurrentTotalBill = CurrentTotalBill - DeletedProductTotal;

                        ProductsGridView.Rows.RemoveAt(e.RowIndex);
                        txt_grand_total.Text = CurrentTotalBill.ToString();
                    }
                    else if (ProductsGridView.Columns[e.ColumnIndex].Name == "Increase")
                    {

                        try
                        {
                            // MessageBox.Show("Product Alraedy Exists in Datagrid view at Index : " + RowIndex);
                            int Quantity = Convert.ToInt32(ProductsGridView.Rows[e.RowIndex].Cells["ProductQuantityColumn"].Value);
                            decimal Price = Convert.ToInt32(ProductsGridView.Rows[e.RowIndex].Cells["ProductPriceColumn"].Value);

                            Quantity++;

                            /////////////<Do thisssss...... Important.. Have decimal part in the total price>
                            double TotalPrice = Convert.ToDouble(Quantity * Price);

                            ProductsGridView.Rows[e.RowIndex].Cells["ProductQuantityColumn"].Value = Quantity;
                            ProductsGridView.Rows[e.RowIndex].Cells["TotalPriceColumn"].Value = TotalPrice;

                            txt_grand_total.Text = CalculateTotalBill(ProductsGridView).ToString();
                        }
                        catch (Exception)
                        {


                        }
                    }
                    else if (ProductsGridView.Columns[e.ColumnIndex].Name == "Decrease")
                    {
                        try
                        {
                            // MessageBox.Show("Product Alraedy Exists in Datagrid view at Index : " + RowIndex);
                            int Quantity = Convert.ToInt32(ProductsGridView.Rows[e.RowIndex].Cells["ProductQuantityColumn"].Value);
                            decimal Price = Convert.ToInt32(ProductsGridView.Rows[e.RowIndex].Cells["ProductPriceColumn"].Value);
                            Quantity--;
                            if (Quantity == 0 )
                            {
                               
                                ProductsGridView.Rows.RemoveAt(ProductsGridView.CurrentRow.Index);

                            }
                            else
                            {
                                double TotalPrice = Convert.ToDouble(Quantity * Price);
                                ProductsGridView.Rows[e.RowIndex].Cells["ProductQuantityColumn"].Value = Quantity;
                                ProductsGridView.Rows[e.RowIndex].Cells["TotalPriceColumn"].Value = TotalPrice;
                                txt_grand_total.Text = CalculateTotalBill(ProductsGridView).ToString();
                       
                               
                            }


                            //txt_grand_total.Clear();
                            /////////////<Do thisssss...... Important.. Have decimal part in the total price>
                            
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void radio_take_CheckedChanged(object sender, EventArgs e)
        {
            combo_table.Enabled = false;
            if (radio_take.Checked == true)
            {
                combo_table.Text = "";
                combo_table.Text = "Take Away";
                SaveBtn.Hide();
                SaveAdvance.Hide();
                Delivery_btn.Hide();
                button1.Hide();
                btn_process.Show();
                button4.Show();
                clear();
            }
        }
        void Tables()
        {
            DataSet ds = db.Select("Select Table_Name from tables ORDER BY Table_ID ");
            combo_table.DataSource = ds.Tables["tbl1"].DefaultView;
            combo_table.DisplayMember = "Table_Name";
        }
        private void radio_table_CheckedChanged(object sender, EventArgs e)
        {
            combo_table.Enabled = true;
            if (radio_table.Checked == true)
            {
                Tables();
               // checkzero = true;
                //combo_table.Text = "";
               // combo_table.Text = "Table No";
               
                SaveBtn.Show();
                SaveAdvance.Hide();
                Delivery_btn.Hide();
                button1.Hide();
                btn_process.Show();
                button4.Show();
                clear();
            }
        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            get_id();
            try
            {
                if (combo_table.Text == "")
                {
                    MessageBox.Show("Please Select Table or Deal.", "POS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (ProductsGridView.Rows.Count > 0)
                    {
                        for (int i = 0; i < ProductsGridView.Rows.Count - 1; i++)
                        {

                            db.ExecuteQuery("insert into sale (Batch_ID,Name,Phone_No,Address,item_code,item_name,quantity,price,bill,Food_Type,total_amount,Paid_Amount,Discount,date,Status,Table_No,Payment) values('" + textBox1.Text + "','" + txt_custname.Text + "','" + textBox4.Text + "','" +textAddress.Text+"','" + ProductsGridView.Rows[i].Cells[0].Value + "','" + ProductsGridView.Rows[i].Cells[1].Value + "','" + ProductsGridView.Rows[i].Cells[3].Value + "','" + ProductsGridView.Rows[i].Cells[2].Value + "','" + ProductsGridView.Rows[i].Cells[4].Value + "','" + ProductsGridView.Rows[i].Cells[5].Value + "','" + txt_grand_total.Text + "','" + txt_grand_total.Text + "',0,'" + dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss") + "','S','" + combo_table.Text + "',0)");


                        }


                        get_id();//** to get batch ID
                        txt_grand_total.Text = "0";


                    }
                }

            }//**** End Try
            catch (Exception ex)
            {

            }///***** End catch    
            ///
            MessageBox.Show("Data Saved");
            clear();
        }

        private void combo_table_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = db.Select("select * from sale where Table_No='" + combo_table.Text + "' and payment=0");
            if (ds != null && ds.Tables["tbl1"].Rows.Count > 0 && checktable == true )
            {
                MessageBox.Show("Order is Already Booked.", "POS", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //  checktable = true;
            }
            else if (combo_table.SelectedIndex == 00)
            {
                MessageBox.Show("Please Select Other Table.", "POS", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            //pictureBox5.Hide();
            //pictureBox4.Show();
            //Sidepanel.Visible = false;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            pictureBox4.Show();
            Sidepanel.Visible = true;
         //   pictureBox5.Show();
        }

        private void Sidepanel_Paint(object sender, PaintEventArgs e)
        {
            list_Box2();
            list_Box();
            list_box3();
        }

        private void Advance_radio_CheckedChanged(object sender, EventArgs e)
        {
            combo_table.Enabled = false;
            if (Advance_radio.Checked == true)
            {

                combo_table.Text = "Advance";
                SaveBtn.Hide();
                Delivery_btn.Hide();
                SaveAdvance.Show();
                button1.Hide();
                btn_process.Show();
                button4.Show();
                clear();
            }
        }

        private void SaveAdvance_Click(object sender, EventArgs e)
        {
            try
            {
                if (combo_table.Text == "")
                {
                    MessageBox.Show("Please Select Table or Deal.", "POS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (ProductsGridView.Rows.Count > 0)
                    {
                        for (int i = 0; i < ProductsGridView.Rows.Count - 1; i++)
                        {

                            db.ExecuteQuery("insert into sale (Batch_ID,Name,Phone_No,Address,item_code,item_name,quantity,price,bill,Food_Type,total_amount,Paid_Amount,Discount,date,Status,Table_No,Payment) values('" + textBox1.Text + "','" + txt_custname.Text + "','" + textBox4.Text + "','" + textAddress.Text + "','" + ProductsGridView.Rows[i].Cells[0].Value + "','" + ProductsGridView.Rows[i].Cells[1].Value + "','" + ProductsGridView.Rows[i].Cells[3].Value + "','" + ProductsGridView.Rows[i].Cells[2].Value + "','" + ProductsGridView.Rows[i].Cells[4].Value + "','" + ProductsGridView.Rows[i].Cells[5].Value + "','" + txt_grand_total.Text + "','" + txt_grand_total.Text + "',0,'" + dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss") + "','S','" + combo_table.Text + "',0)");


                        }


                        get_id();//** to get batch ID
                        txt_grand_total.Text = "0";


                    }
                }

            }//**** End Try
            catch (Exception ex)
            {

            }///***** End catch    
            ///
            MessageBox.Show("Data Saved");
            clear();
        }
        void clear()
        {

            txt_custname.Clear();
            textBox4.Clear();
            ProductsGridView.Rows.Clear();
            txt_grand_total.Clear();
            textAddress.Clear();
            get_id();
        }

        private void Delivery_btn_Click(object sender, EventArgs e)
        {
            //get_id();
            try
            {
                if (combo_table.Text == "")
                {
                    MessageBox.Show("Please Select Table or Deal.", "POS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (ProductsGridView.Rows.Count > 0)
                    {
                        for (int i = 0; i < ProductsGridView.Rows.Count - 1; i++)
                        {

                            db.ExecuteQuery("insert into sale (Batch_ID,Name,Phone_No,Address,item_code,item_name,quantity,price,bill,Food_Type,total_amount,Paid_Amount,Discount,date,Status,Table_No,Payment) values('" + textBox1.Text + "','" + txt_custname.Text + "','" + textBox4.Text + "','" + textAddress.Text + "','" + ProductsGridView.Rows[i].Cells[0].Value + "','" + ProductsGridView.Rows[i].Cells[1].Value + "','" + ProductsGridView.Rows[i].Cells[3].Value + "','" + ProductsGridView.Rows[i].Cells[2].Value + "','" + ProductsGridView.Rows[i].Cells[4].Value + "','" + ProductsGridView.Rows[i].Cells[5].Value + "','" + txt_grand_total.Text + "','" + txt_grand_total.Text + "',0,'" + dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss") + "','S','" + combo_table.Text + "',0)");


                        }

                        CR_Form("select * from Sale where Batch_ID=" + textBox1.Text + "");
                        get_id();//** to get batch ID
                        txt_grand_total.Text = "0";


                    }
                }

            }//**** End Try
            catch (Exception ex)
            {

            }///***** End catch    
            ///
            MessageBox.Show("Data Saved");
            get_id();
            clear();
        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            Delivery_btn.Hide();
            btn_process.Show();
            if (listBox3.Text != "System.Data.DataRowView" && !string.IsNullOrEmpty(listBox3.Text))
            {
                DataSet ds = db.Select("Select * from sale Where Batch_ID='" + listBox3.Text + "' and Payment=0 ");
                if (ds.Tables["tbl1"].Rows.Count > 0)
                {
                    combo_table.Text = ds.Tables["tbl1"].Rows[0]["Table_No"].ToString();
                    textBox1.Text = ds.Tables["tbl1"].Rows[0]["Batch_ID"].ToString();
                    txt_custname.Text = ds.Tables["tbl1"].Rows[0]["Name"].ToString();
                    textBox4.Text = ds.Tables["tbl1"].Rows[0]["Phone_No"].ToString();
                    txt_grand_total.Text = ds.Tables["tbl1"].Rows[0]["Total_Amount"].ToString();
                    //clear();
                    ProductsGridView.Rows.Clear();


                    for (int i = 0; i < ds.Tables["tbl1"].Rows.Count; i++)
                    {
                        {
                            //string item_name = ProductsGridView.Rows[i].Cells[1].Value.ToString();
                            string ID = ds.Tables["tbl1"].Rows[i]["Item_Code"].ToString();
                            string item_name = ds.Tables["tbl1"].Rows[i]["Item_Name"].ToString();
                            string price = ds.Tables["tbl1"].Rows[i]["Price"].ToString();
                            string quantity = ds.Tables["tbl1"].Rows[i]["Quantity"].ToString();
                            string Product_Total = ds.Tables["tbl1"].Rows[i]["Bill"].ToString();
                            string type = ds.Tables["tbl1"].Rows[i]["Food_Type"].ToString();
                            ProductsGridView.Rows.Add(ID, item_name, price, quantity, Product_Total, type);

                        }
                    }

                }
            }

        }

        private void listBox3_DoubleClick(object sender, EventArgs e)
        {
            list_box3();
        }

        private void Sidepanel_MouseHover(object sender, EventArgs e)
        {
          //  Sidepanel.Visible = false;
        //    list_Box2();
        //    list_Box();
        //    list_box3();
        }

        private void btn_process_Click(object sender, EventArgs e)
        {
            //get_id();
            if (checkpayment == true)
            {
                DataSet ds = db.Select("delete from sale where Batch_ID = '" + textBox1.Text + "'");
                process_save();
                combo_table.Text = "Take Away";
                txt_custname.Clear();
                textBox4.Clear();
                txt_grand_total.Clear();
                ProductsGridView.Rows.Clear();

            }
            else
            {

            }

            // SAVE IN DB
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    //db.Select("delete from sale where batch_ID='" + textBox1.Text + "'");
            //    db.ExecuteQuery("update Sale set Status='k' where batch_ID='" + textBox1.Text + "' and payment=0");
            //    MessageBox.Show("Order Canceled.", "POS", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    list_Box();// Advance order refresh
            //    get_id();

            //}
            //catch (Exception)
            //{

            //}

        }
        string conse;

        public ArrayList RetreiveAllCategoriesFromDatabase()
        {

            ArrayList CategoriesList = new ArrayList();
            conse = ConnectionString.ConString;
            using (SqlConnection connection = new SqlConnection(conse))
            {
                SqlCommand command = new SqlCommand("SELECT ID, CategoryName FROM Categories where Status=1;", connection);
                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int CategoryID = reader.GetInt32(0);
                        string CategoryName = reader.GetString(1);
                        // byte[] CategoryPicture = (byte[])reader[2];
                        CategoriesList.Add(new Details() { ID = CategoryID, Name = CategoryName });
                    }
                }
                reader.Close();

                return CategoriesList;
            }
        }
        //***** ON LOAD EVENT ///**********++++++++++
        private void POS_Load(object sender, EventArgs e)
        {
             //LocalConnection();
           // list_view();//++++*** :List VIEW on Load
            get_id();//* TO get Batch ID on load
           // low_stock();//** TO ENABLE LOW STOCK WARNING
            get_name();//*** TO GET PRINTER NAME FOR RECEIPT 
            list_Box();
            list_Box2();
            list_box3();
            // UpdateOrder();
            printer = label18.Text;
            clear();
            txt_grand_total.Clear();
            
            if (radio_take.Checked == true)
            {
                combo_table.Text = "";
                combo_table.Text = "Take Away";
            }
            Delivery_btn.Visible = false;
            SaveAdvance.Visible = false;
            SaveBtn.Visible = false;


            ArrayList AllCategories = RetreiveAllCategoriesFromDatabase();
            DataAccess dataAccess = new DataAccess();
            DataTable tbl1 = dataAccess.getallCategory();
            //foreach (Details Category in AllCategories)
            //{
            for (int i = 0; i < tbl1.Rows.Count; i++)
            {
                try
                {



                    Button btn = new Button();
                    btn.Text = tbl1.Rows[i][1].ToString();
                    btn.Size = new System.Drawing.Size(80, 80);
                    btn.ForeColor = Color.Black;
                    btn.Font = new System.Drawing.Font("Segoe UI Black", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    string path = Application.StartupPath + @"\" + tbl1.Rows[i][5].ToString() + @"\" + tbl1.Rows[i][4].ToString();
                    btn.Image = Image.FromFile(path);
                    btn.Image = new Bitmap(btn.Image, btn.Size);

                    // btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                    //MemoryStream ms = new MemoryStream(Category.Picture);
                    //btn.Image = Image.FromStream(ms);
                    // btn.Image = new Bitmap(btn.Image, btn.Size);
                    //btn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
                    //btn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
                    btn.Tag = tbl1.Rows[i][0];

                    CategoriesFlowPanel.Controls.Add(btn);
                    btn.Click += btn_Click;
                    //btn.Click += CategoryButtonClick;
                }
                catch (Exception)
                {

                    //throw;
                }


            }
            {
               
            }
        }
        //************************************************************************************
        //******************************************************************************************
        // All PRODUCTS GET ON THE BASE OF CATEGORYYYYYY
        public ArrayList RetreiveProductsFromCategory(int CategoryID)
        {
            ArrayList ProductsList = new ArrayList();
            conse = ConnectionString.ConString;
            using (SqlConnection connection = new SqlConnection(conse))
            {
                SqlCommand command = new SqlCommand("SELECT Deal_ID, Deal_Name,Deal_Detail,Price,Type,FileName,FilePath FROM Deals where CatogryID = '" + CategoryID + "' and Status=1;", connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        int ProductID = reader.GetInt32(0);
                        string ProductName = reader.GetString(1);
                        string ProductDescription = reader.GetString(2);
                        int ProductPrice = reader.GetInt32(3);
                        string ProductType = reader.GetString(4);
                        // byte[] ProductPicture = (byte[])reader[5];
                        string FileName = reader.GetString(5);
                        string FilePath = reader.GetString(6);

                        ProductsList.Add(new Details() { ID = ProductID, Name = ProductName, Description = ProductDescription, Price = ProductPrice, protype = ProductType, FileName = FileName, FilePath = FilePath });
                    }
                }
                reader.Close();

                return ProductsList;
            }
        }

        void btn_Click(object sender, EventArgs e)
        {
            ProductsFlowPanel.Controls.Clear();
            Button btn = (Button)sender;
            int CategoryID = Convert.ToInt32(btn.Tag);
            DataAccess dataAccess = new DataAccess();
            DataTable tbl1 = dataAccess.AllDeals(CategoryID);


            //foreach (Details Product in RetreiveProductsFromCategory(CategoryID))
            for (int i = 0; i < tbl1.Rows.Count; i++)
            {
                Button probut = new Button();
                probut.Text = tbl1.Rows[i][1].ToString();
                probut.Size = new System.Drawing.Size(80, 80);
                probut.ForeColor = Color.Black;
                string path = Application.StartupPath + @"\" + tbl1.Rows[i][9].ToString() + @"\" + tbl1.Rows[i][8].ToString();
                probut.Image = Image.FromFile(path);
                probut.Image = new Bitmap(probut.Image, btn.Size);



                // MemoryStream ms = new MemoryStream(Product.Picture);
                // probut.Image = Image.FromStream(ms);
                // probut.Image = new Bitmap(probut.Image, probut.Size);
                probut.BackColor = Color.Red;
                probut.Tag = tbl1.Rows[i][0];


                ProductsFlowPanel.Controls.Add(probut);
                probut.Click += probut_Click;
                //CategoriesFlowPanel.Controls.Add(probut);
                //probut.Click += probut_Click;
            }
        }

        void probut_Click(object sender, EventArgs e)
        {
            Button probut = sender as Button;
            int ProductID = Convert.ToInt32(probut.Tag);
            DataAccess da = new DataAccess();
            Details Product_Detail = da.RetreiveProductDetails(ProductID);
            if (CheckProductAlreadyAdded(ProductID))
            {
                // MessageBox.Show("Product Alraedy Exists in Datagrid view at Index : " + RowIndex);
                int Quantity = Convert.ToInt32(ProductsGridView.Rows[RowIndex].Cells["ProductQuantityColumn"].Value);
                decimal Price = Convert.ToInt32(ProductsGridView.Rows[RowIndex].Cells["ProductPriceColumn"].Value);

                Quantity++;

                /////////////<Do thisssss...... Important.. Have decimal part in the total price>
                double TotalPrice = Convert.ToDouble(Quantity * Price);

                ProductsGridView.Rows[RowIndex].Cells["ProductQuantityColumn"].Value = Quantity;
                ProductsGridView.Rows[RowIndex].Cells["TotalPriceColumn"].Value = TotalPrice;

                txt_grand_total.Text = CalculateTotalBill(ProductsGridView).ToString();
            }
            else
            {
                ProductsGridView.Rows.Add(ProductID, Product_Detail.Name, Product_Detail.Price, 1, Product_Detail.Price * 1, Product_Detail.protype);

                txt_grand_total.Text = CalculateTotalBill(ProductsGridView).ToString();
            }
        }
        public bool CheckProductAlreadyAdded(int ProductID)
        {
            foreach (DataGridViewRow Row in ProductsGridView.Rows)
            {
                if (Convert.ToInt32(Row.Cells["ProductIDColumn"].Value) == ProductID)
                {
                    RowIndex = Row.Index;
                    return true;
                }
            }
            return false;
        }
        public decimal CalculateTotalBill(DataGridView ProductsGridView)
        {
            decimal TotalBill = 0;

            foreach (DataGridViewRow Row in ProductsGridView.Rows)
            {
                decimal ProductTotal = Convert.ToDecimal(Row.Cells["TotalPriceColumn"].Value);

                TotalBill = TotalBill + ProductTotal;
            }

            return TotalBill;
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void txt_grand_total_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_custname_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            //refresh();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbl_custmoer_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataSet ds = db.Select("delete from sale where Batch_ID = '" + textBox1.Text + "'");

            if (ProductsGridView.Rows.Count > 0)
            {
                for (int i = 0; i < ProductsGridView.Rows.Count - 1; i++)
                {
                    db.ExecuteQuery("insert into sale (Batch_ID, Name, Phone_No ,Address,item_code,item_name,quantity,price,bill,Food_Type,total_amount,Paid_Amount,Discount,date,Status,Table_No,Payment) values('" + textBox1.Text + "','" + txt_custname.Text + "','" + textBox4.Text + "','" + textAddress.Text + "','" + ProductsGridView.Rows[i].Cells[0].Value + "','" + ProductsGridView.Rows[i].Cells[1].Value + "','" + ProductsGridView.Rows[i].Cells[3].Value + "','" + ProductsGridView.Rows[i].Cells[2].Value + "','" + ProductsGridView.Rows[i].Cells[4].Value + "','" + ProductsGridView.Rows[i].Cells[5].Value + "','" + txt_grand_total.Text + "','" + txt_grand_total.Text + "',0,'" + dateTimePicker1.Value.ToString("yyyy-MM-dd HH:mm:ss") + "','S','" + combo_table.Text + "',1)");
                    // ProductsGridView.Rows.Clear();
                    // CustomerName = null;
                }
                MessageBox.Show("Data Saved");
            }
            ProductsGridView.Rows.Clear();
            clear();
            
           
        }
        public void refresh()
        {
            string query = "select * from sale where Phone_No = '" + textBox4.Text + "'";
            DataSet ds = db.Select(query);

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {

                string Name = ds.Tables[0].Rows[i][2].ToString();
                string Address = ds.Tables[0].Rows[i][4].ToString();
                txt_custname.Text = Name;
                textAddress.Text = Address;
            }
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                //db.Select("delete from sale where batch_ID='" + textBox1.Text + "'");
                db.ExecuteQuery("update Sale set Status='k' where batch_ID='" + textBox1.Text + "' and payment=0");
                MessageBox.Show("Order Canceled.", "POS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                list_Box();// Advance order refresh
                //get_id();

            }
            catch (Exception)
            {

            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            //list_Box();
            //list_Box2();
            //list_box3();
            listBox1.DataSource = null;
            listBox2.DataSource = null;
            listBox3.DataSource = null;
        }

        private void textBox4_Enter(object sender, EventArgs e)
        {
            //refresh();

        }

        private void btnsrc2_Click(object sender, EventArgs e)
        {
            //refresh();
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            //refresh();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            get_id();//* TO get Batch ID on load
            // low_stock();//** TO ENABLE LOW STOCK WARNING
            get_name();//*** TO GET PRINTER NAME FOR RECEIPT 
            list_Box();
            list_Box2();
            list_box3();
            // UpdateOrder();
            printer = label18.Text;
            clear();
            txt_grand_total.Clear();
            radio_take.Checked = true;
            if (radio_take.Checked == true)
            {
                combo_table.Text = "";
                combo_table.Text = "Take Away";
            }
            Delivery_btn.Visible = false;
            SaveAdvance.Visible = false;
            SaveBtn.Visible = false;

        }

        private void button6_Click(object sender, EventArgs e)
        {
            list_Box();
            list_Box2();
            list_box3();
        }

        

        private void Sidepanel_MouseClick(object sender, MouseEventArgs e)
        {
            Sidepanel.Visible = false;
        }

      
       

      

        private void listBox2_MouseLeave(object sender, EventArgs e)
        {
            Sidepanel.Visible = false;
        }

        private void btnsrc2_Click_1(object sender, EventArgs e)
        {
            refresh();
        }

        private void listBox1_MouseLeave(object sender, EventArgs e)
        {
            //Sidepanel.Visible = false;
        }

        private void listBox3_MouseLeave(object sender, EventArgs e)
        {
           // Sidepanel.Visible = false;
        }

        private void textRecive_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textRecive.Text) == false)
            {
                int paid = Convert.ToInt32(textRecive.Text);
                int cost = Convert.ToInt32(txt_grand_total.Text);
                int change = cost - paid;
                textReturn.Text = change.ToString();
            }
            else
            {
                textReturn.Clear();
            }

        }

     
    }
}

