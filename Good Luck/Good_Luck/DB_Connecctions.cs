﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using DAL;
namespace Bakery_Management_System
{
    class DB_Connecctions
    {
         //public string ConnectionString = "Data Source=DESKTOP-Q77RPAB;Initial Catalog=Good_Luck;Integrated Security=True";
       // public string ConnectionString = "Data Source=LENOVO-PC;Initial Catalog=Good_Luck;Integrated Security=True";
        //public string ConnectionString = "Data Source=DESKTOP-4UE1T9R;Initial Catalog=Good_Luck;Integrated Security=True";
        //string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["HDS"].ConnectionString;

        SqlConnection con;
        //SqlCommand cmd;
        SqlDataAdapter da;
        DataSet ds;
        DataTable dt;
        
        public DB_Connecctions ()
        {
           
            con = new SqlConnection(ConnectionString.ConString);
        }
        public void ExecuteQuery(string query)
        {
            con.Open();
            SqlCommand cmd = new SqlCommand(query,con);
            cmd.ExecuteNonQuery();
            con.Close();
        }
        public DataSet Select(string query)
        {
            SqlCommand cmd = new SqlCommand(query,con);
            da = new SqlDataAdapter(cmd);
            ds = new DataSet();
            da.Fill(ds, "tbl1");
            return ds;
        }
        public DataTable list_process(string query)
        {
            con.Open();
            da = new SqlDataAdapter(query, con);
            dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        internal DataSet Select1(string query, string TableName)
        {
            SqlCommand cmd = new SqlCommand(query, con);
            da = new SqlDataAdapter(cmd);
            ds = new DataSet();
            da.Fill(ds, TableName);
            return ds;
        }
    }
}
