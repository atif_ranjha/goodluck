﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using DAL;
using System.Drawing.Imaging;


namespace Bakery_Management_System
{
    public partial class Employees : UserControl
    {
        public Employees()
        {
            InitializeComponent();
        }
        DB_Connecctions db = new DB_Connecctions();
        string con;
        private void Employees_Load(object sender, EventArgs e)
        {
            getdepartment();
            getdesgnation();
            bindgridview();
        }

        private void UploadPictureButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Title = "Select Image file..";
            ofd.DefaultExt = ".jpg";
            ofd.Filter = "Media Files|*.jpg;*.png;*.gif;*.bmp;*.jpeg|All Files|*.*";

            DialogResult result = ofd.ShowDialog();
            if (result == DialogResult.OK)
            {
                /*if picture selected then load in the picture box*/
                pictureBox1.Load(ofd.FileName);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Title = "Select Image file..";
            ofd.DefaultExt = ".jpg";
            ofd.Filter = "Media Files|*.jpg;*.png;*.gif;*.bmp;*.jpeg|All Files|*.*";

            DialogResult result = ofd.ShowDialog();
            if (result == DialogResult.OK)
            {
                /*if picture selected then load in the picture box*/
                pictureBox2.Load(ofd.FileName);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Title = "Select Image file..";
            ofd.DefaultExt = ".jpg";
            ofd.Filter = "Media Files|*.jpg;*.png;*.gif;*.bmp;*.jpeg|All Files|*.*";

            DialogResult result = ofd.ShowDialog();
            if (result == DialogResult.OK)
            {
                /*if picture selected then load in the picture box*/
                pictureBox3.Load(ofd.FileName);
            }
        }

        public void getdepartment()
        {
            string query = "select * from Department";
            DataSet ds = db.Select(query);
            if (ds != null)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    comboDep.Items.Add(ds.Tables[0].Rows[i][1].ToString());
                }
            }
        }
        public void getdesgnation()
        {
            string query = "select * from Designation";
            DataSet ds = db.Select(query);
            if (ds != null)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    comboDes.Items.Add(ds.Tables[0].Rows[i][1].ToString());
                }
            }
        }

        void dataMapping(DataGridViewRow row)
        {
            txtID.Text = row.Cells[0].Value.ToString();
            txtName.Text = row.Cells[1].Value.ToString();
            txtCNIC.Text = row.Cells[2].Value.ToString();
            comboDep.Text = row.Cells[6].Value.ToString();
            comboDes.Text = row.Cells[7].Value.ToString();
            byte[] pict = (byte[])(row.Cells[3].Value);
            MemoryStream ms = new MemoryStream(pict);
            pictureBox1.Image = Image.FromStream(ms);
            pictureBox2.Image = Image.FromStream(ms);
            pictureBox3.Image = Image.FromStream(ms);

        }

        private void btn_save_Click(object sender, EventArgs e)
        {

            try
            {
                con = ConnectionString.ConString;
                SqlConnection conn = new SqlConnection(con);
                conn.Open();
                SqlCommand cmd = new SqlCommand("Insert into EmployeesData(Id, Name,CNIC,Department,Designation,Picture,Picture_CNIC,Pitcure_Contract,Barcode) values('" + txtID.Text + "','" + txtName.Text + "','" + txtCNIC.Text + "','" + comboDep.Text + "','" + comboDes.Text + "',@pic1,@pic2,@pic3,@pic4)", conn);
                MemoryStream ms = new MemoryStream();
                /*saving the image in raw format from picture box*/
                pictureBox1.Image.Save(ms, pictureBox1.Image.RawFormat);
                pictureBox2.Image.Save(ms, pictureBox2.Image.RawFormat);
                pictureBox3.Image.Save(ms, pictureBox3.Image.RawFormat);
                pictureBox4.Image.Save(ms, pictureBox3.Image.RawFormat);

                /*Array of Binary numbers that have been converted*/
                byte[] CategoryPicture1 = ms.GetBuffer();
                byte[] CategoryPicture2 = ms.GetBuffer();
                byte[] CategoryPicture3 = ms.GetBuffer();
                byte[] CategoryPicture4 = ms.GetBuffer();
                cmd.Parameters.AddWithValue("@Pic1", CategoryPicture1);
                cmd.Parameters.AddWithValue("@Pic2", CategoryPicture2);
                cmd.Parameters.AddWithValue("@Pic3", CategoryPicture3);
                cmd.Parameters.AddWithValue("@Pic4", CategoryPicture4);
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("New Employee Data Saved Succefully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch
            {

            }
            resetbtns();

        }

        private void comboDep_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string barcode = txtbar.Text;
            Bitmap bitmap = new Bitmap(barcode.Length * 40, 150);
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                Font oFont = new System.Drawing.Font("IDAHC39M Code 39 Barcode", 20);
                PointF point = new PointF(2f, 2f);
                SolidBrush black = new SolidBrush(Color.Black);
                SolidBrush white = new SolidBrush(Color.White);
                graphics.FillRectangle(white, 0, 0, bitmap.Width, bitmap.Height);
                graphics.DrawString("*" + barcode + "*", oFont, black, point);
            }
            using (MemoryStream ms = new MemoryStream())
            {
                bitmap.Save(ms, ImageFormat.Png);
                pictureBox4.Image = bitmap;
                pictureBox4.Height = bitmap.Height;
                pictureBox4.Width = bitmap.Width;
            }
        }
        
        private void button5_Click(object sender, EventArgs e)
        {
            db.ExecuteQuery("update EmployeesData set Status=0 where Id='" + txtID.Text + "'");
            MessageBox.Show("EmployeesData Deleted Succefully.", "EmployeesData Section", MessageBoxButtons.OK, MessageBoxIcon.Information);
            resetbtns();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                con = ConnectionString.ConString;
                SqlConnection conn = new SqlConnection(con);
                conn.Open();
                SqlCommand cmd = new SqlCommand("Update EmployeesData set Name='" + txtName.Text + "',CNIC='" + txtCNIC.Text + "',Picture=@pic1,Picture_CNIC=@pic2,Pitcure_Contract=@pic3,Barcode=@pic4 where Id='" + txtID.Text + "'", conn);
                MemoryStream ms = new MemoryStream(); 
                /*saving the image in raw format from picture box*/
                pictureBox1.Image.Save(ms, pictureBox1.Image.RawFormat);
                pictureBox2.Image.Save(ms, pictureBox2.Image.RawFormat);
                pictureBox3.Image.Save(ms, pictureBox3.Image.RawFormat);
                pictureBox4.Image.Save(ms, pictureBox3.Image.RawFormat);

                /*Array of Binary numbers that have been converted*/
                byte[] CategoryPicture1 = ms.GetBuffer();
                byte[] CategoryPicture2 = ms.GetBuffer();
                byte[] CategoryPicture3 = ms.GetBuffer();
                byte[] CategoryPicture4 = ms.GetBuffer();
                cmd.Parameters.AddWithValue("@Pic1", CategoryPicture1);
                cmd.Parameters.AddWithValue("@Pic2", CategoryPicture2);
                cmd.Parameters.AddWithValue("@Pic3", CategoryPicture3);
                cmd.Parameters.AddWithValue("@Pic4", CategoryPicture4);
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("New Employee Data Updated Succefully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch
            {

            }

            resetbtns();

        }

        void bindgridview()
    {

      

        DataSet ds = db.Select("select Id,Name,CNIC,Picture,Picture_CNIC,Pitcure_Contract,Department,Designation from EmployeesData");
        dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
    } 

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
                dataMapping(row);


            }
        }

        void resetbtns()
        {
            txtbar.Clear();
            txtCNIC.Clear();
            txtID.Clear();
            txtName.Clear();
            comboDep.ResetText();
            comboDes.ResetText();
            pictureBox1.Image = null;
            pictureBox2.Image = null;
            pictureBox3.Image = null;
            pictureBox4.Image = null;
            pictureBox5.Image = null;
        
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
    }
}
