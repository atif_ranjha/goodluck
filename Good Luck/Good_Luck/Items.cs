﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using DAL;
namespace Bakery_Management_System
{
    public partial class Items : UserControl
    {
        public Items()
        {
            InitializeComponent();
        
        }
        // *****+++++++++++++++ DATABASE CONNECTION CLASS GLOBAL ++++++++++++*******//
        DB_Connecctions db = new DB_Connecctions();
        string con;
        // *****+++++++++++++++ TO REFRESH GRID VIEW ++++++++++++*******//
        public void refre()
        {
            DataSet ds = db.Select("select [Item_Code],[Item_Name],[Item_Description],[Unit],[Item_Stock],Item_Price as 'Low Stock Limit' from Items");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
        }
        // *****+++++++++++++++ TO GET ITEM CODE ++++++++++++*******//
        public void get_id()
        {
            DataSet ds = db.Select("select isnull(max(cast(Item_code as int)),0)+1 from Items");
            txt_item_Code.Text = ds.Tables["tbl1"].Rows[0][0].ToString();
        }
        // *****+++++++++++++++ TO COUNT ITEMS IN STOCK Form ++++++++++++*******//
        public void count_item()
        {
            DataSet ds = db.Select("select count(item_code) from Items");
            lbl_count.Text = ds.Tables["tbl1"].Rows[0][0].ToString();
        }
        // *****+++++++++++++++ TO Clear Form ++++++++++++*******//
        public void clear()
        {
            txt_discription.Clear();
            //txt_item_Code.Clear();
            txt_item_name.Clear();
            txt_lowstock.Clear();
            txt_stock.Clear();
        }
        // *****+++++++++++++++ FORM LOAD EVENTS AND FUNCTIONS ++++++++++++*******//
        private void Items_Load(object sender, EventArgs e)
        {
            refre();// TO Load Gridview on Form Load
            get_id();// TO GET ITEM CODE on FORM LOAD
            count_item();//TO COUNT ITEMS IN STOCK
            //Enable and Disable button on form Load
            btn_save.Enabled = true;
            btn_update.Enabled = false;
            btn_delete.Enabled = false;
        }
        // *****+++++++++++++++ SAVE DATA BUTTON ++++++++++++*******//
        private void btn_save_Click(object sender, EventArgs e)
        {
            if(txt_item_name.Text=="" || txt_discription.Text=="" || txt_lowstock.Text=="" || txt_stock.Text=="")
            {
                MessageBox.Show("Please! Fill all the Fields.","Items",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    con = ConnectionString.ConString;
                    SqlConnection conn = new SqlConnection(con);
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("insert into items (item_code,Item_Name,Item_description,Unit,item_stock,Item_Price) values('" + txt_item_Code.Text + "','" + txt_item_name.Text + "','" + txt_discription.Text + "','" + combo_item.Text + "','" + txt_stock.Text + "','"+txt_lowstock.Text+"')", conn);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    MessageBox.Show("New Item Saved Succefully.", "Items Section", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    clear();
                    get_id();
                    refre();
                    count_item();
                    txt_item_name.Focus();
                }
                catch (Exception)
                {

                }
            }
            
        }
        // *****+++++++++++++++ Update DATA BUTTON ++++++++++++*******//
        private void btn_update_Click(object sender, EventArgs e)
        {
            try
            {
                con = ConnectionString.ConString;
                SqlConnection conn = new SqlConnection(con);
                conn.Open();
                SqlCommand cmd = new SqlCommand("update items set item_name='"+txt_item_name.Text+"',item_description='"+txt_discription.Text+"',Unit='"+combo_item.Text+"',Item_Price='"+txt_lowstock.Text+"' where item_code='"+txt_item_Code.Text+"'", conn);
                cmd.ExecuteNonQuery();
                conn.Close();
                //db.ExecuteQuery("update items set item_name='"+txt_item_name.Text+"',item_description='"+txt_discription.Text+"',item_price='"+txt_price.Text+"' where item_code='"+txt_item_Code.Text+"'");
                MessageBox.Show("Item Updated Succefully.", "Items Section", MessageBoxButtons.OK, MessageBoxIcon.Information);
                clear();
                get_id();
                refre();
                count_item();
                btn_save.Enabled = true;
                btn_delete.Enabled = false;
                btn_update.Enabled = false;
                txt_item_name.Focus();
                txt_stock.Enabled = true;
            }
            catch (Exception)
            {
                
            }
        }
        // *****+++++++++++++++ Delete DATA BUTTON ++++++++++++*******//
        private void btn_delete_Click(object sender, EventArgs e)
        {
            //try
            //{
                db.ExecuteQuery("delete from items where item_code='" + txt_item_Code.Text + "'");
                MessageBox.Show("Item Deleted Succefully.", "Items Section", MessageBoxButtons.OK, MessageBoxIcon.Information);
                clear();
                get_id();
                refre();
                count_item();
                btn_save.Enabled = true;
                btn_delete.Enabled = false;
                btn_update.Enabled = false;
                txt_item_name.Focus();
                txt_stock.Enabled = true;
            //}
            //catch (Exception)
            //{
            //}
        }
        // *****+++++++++++++++ ADD NEW DATA BUTTON ++++++++++++*******//
        private void btn_clear_Click(object sender, EventArgs e)
        {
            clear();
            get_id();
            btn_save.Enabled = true;
            btn_delete.Enabled = false;
            btn_update.Enabled = false;
            txt_stock.Enabled = true;
        }
        // *****+++++++++++++++ SEARCH DATA BUTTON ++++++++++++*******//
        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            DataSet ds = db.Select("select [Item_Code],[Item_Name],[Item_Description],[Unit],[Item_Stock],Item_Price as 'Low Stock Limit' from Items where item_code like '%" + txt_search.Text + "%' or item_name like '%" + txt_search.Text + "%'");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
        }
        // *****+++++++++++++++ GRID VIEW ON CELL CLICK TO FILL FORM ++++++++++++*******//
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                txt_item_Code.Text = row.Cells[0].Value.ToString();
                txt_item_name.Text = row.Cells[1].Value.ToString();
                txt_discription.Text = row.Cells[2].Value.ToString();
                txt_lowstock.Text = row.Cells[5].Value.ToString();
                txt_stock.Text = row.Cells[4].Value.ToString();
                combo_item.Text = row.Cells[3].Value.ToString();
            }
            txt_stock.Enabled = false;
            btn_save.Enabled = false;
            btn_delete.Enabled = true;
            btn_update.Enabled = true;
        }
        //BARCODE GENERATION /////////*************
        private void txt_item_Code_TextChanged(object sender, EventArgs e)
        {
          // barcode();
        }
       
    }
}
