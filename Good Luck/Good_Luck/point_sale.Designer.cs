﻿namespace Bakery_Management_System
{
    partial class Point_Sale
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Point_Sale));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.Delivery_btn = new System.Windows.Forms.Button();
            this.Delivery_radio = new System.Windows.Forms.RadioButton();
            this.SaveAdvance = new System.Windows.Forms.Button();
            this.Advance_radio = new System.Windows.Forms.RadioButton();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.radio_table = new System.Windows.Forms.RadioButton();
            this.radio_take = new System.Windows.Forms.RadioButton();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.menulbl = new System.Windows.Forms.Label();
            this.Sidepanel = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.label33 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label20 = new System.Windows.Forms.Label();
            this.ProductsGridView = new System.Windows.Forms.DataGridView();
            this.ProductIDColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductPriceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductQuantityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPriceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeleteColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.Increase = new System.Windows.Forms.DataGridViewImageColumn();
            this.Decrease = new System.Windows.Forms.DataGridViewImageColumn();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.ProductsFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.CategoriesFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.txt_custname = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txt_grand_total = new System.Windows.Forms.TextBox();
            this.lbl_custmoer = new System.Windows.Forms.Label();
            this.combo_waiter = new System.Windows.Forms.ComboBox();
            this.lbl_waiter = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.label24 = new System.Windows.Forms.Label();
            this.combo_table = new System.Windows.Forms.ComboBox();
            this.button10 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button7 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.btn_process = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn3 = new System.Windows.Forms.DataGridViewImageColumn();
            this.btn_search = new System.Windows.Forms.Button();
            this.txt_serach_batch = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textAddress = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.Sidepanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProductsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.PowderBlue;
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1233, 42);
            this.panel1.TabIndex = 11;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.PowderBlue;
            this.label16.Location = new System.Drawing.Point(703, 9);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(14, 21);
            this.label16.TabIndex = 2;
            this.label16.Text = ".";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Black", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(487, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(164, 32);
            this.label5.TabIndex = 1;
            this.label5.Text = "Point of Sale";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(13, 11);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(100, 20);
            this.label18.TabIndex = 64;
            this.label18.Text = "Printer name";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.Delivery_btn);
            this.tabPage1.Controls.Add(this.textAddress);
            this.tabPage1.Controls.Add(this.Delivery_radio);
            this.tabPage1.Controls.Add(this.SaveAdvance);
            this.tabPage1.Controls.Add(this.Advance_radio);
            this.tabPage1.Controls.Add(this.SaveBtn);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.radio_table);
            this.tabPage1.Controls.Add(this.radio_take);
            this.tabPage1.Controls.Add(this.pictureBox5);
            this.tabPage1.Controls.Add(this.menulbl);
            this.tabPage1.Controls.Add(this.Sidepanel);
            this.tabPage1.Controls.Add(this.ProductsGridView);
            this.tabPage1.Controls.Add(this.pictureBox4);
            this.tabPage1.Controls.Add(this.ProductsFlowPanel);
            this.tabPage1.Controls.Add(this.CategoriesFlowPanel);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.textBox4);
            this.tabPage1.Controls.Add(this.txt_custname);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.txt_grand_total);
            this.tabPage1.Controls.Add(this.lbl_custmoer);
            this.tabPage1.Controls.Add(this.combo_waiter);
            this.tabPage1.Controls.Add(this.lbl_waiter);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.dataGridView2);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.combo_table);
            this.tabPage1.Controls.Add(this.button10);
            this.tabPage1.Controls.Add(this.dateTimePicker1);
            this.tabPage1.Controls.Add(this.button7);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.btn_process);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1240, 484);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "p";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // Delivery_btn
            // 
            this.Delivery_btn.BackColor = System.Drawing.Color.RoyalBlue;
            this.Delivery_btn.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Delivery_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Delivery_btn.Location = new System.Drawing.Point(514, 344);
            this.Delivery_btn.Name = "Delivery_btn";
            this.Delivery_btn.Size = new System.Drawing.Size(130, 52);
            this.Delivery_btn.TabIndex = 137;
            this.Delivery_btn.Text = "Delivery";
            this.Delivery_btn.UseVisualStyleBackColor = false;
            this.Delivery_btn.Click += new System.EventHandler(this.Delivery_btn_Click);
            // 
            // Delivery_radio
            // 
            this.Delivery_radio.AutoSize = true;
            this.Delivery_radio.Location = new System.Drawing.Point(302, 48);
            this.Delivery_radio.Name = "Delivery_radio";
            this.Delivery_radio.Size = new System.Drawing.Size(71, 17);
            this.Delivery_radio.TabIndex = 13;
            this.Delivery_radio.TabStop = true;
            this.Delivery_radio.Text = "Delivery";
            this.Delivery_radio.UseVisualStyleBackColor = true;
            this.Delivery_radio.CheckedChanged += new System.EventHandler(this.Delivery_radio_CheckedChanged);
            // 
            // SaveAdvance
            // 
            this.SaveAdvance.BackColor = System.Drawing.Color.LightSeaGreen;
            this.SaveAdvance.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveAdvance.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.SaveAdvance.Image = ((System.Drawing.Image)(resources.GetObject("SaveAdvance.Image")));
            this.SaveAdvance.Location = new System.Drawing.Point(397, 432);
            this.SaveAdvance.Name = "SaveAdvance";
            this.SaveAdvance.Size = new System.Drawing.Size(119, 52);
            this.SaveAdvance.TabIndex = 136;
            this.SaveAdvance.Text = "Advance";
            this.SaveAdvance.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.SaveAdvance.UseVisualStyleBackColor = false;
            this.SaveAdvance.Click += new System.EventHandler(this.SaveAdvance_Click);
            // 
            // Advance_radio
            // 
            this.Advance_radio.AutoSize = true;
            this.Advance_radio.Location = new System.Drawing.Point(221, 48);
            this.Advance_radio.Name = "Advance_radio";
            this.Advance_radio.Size = new System.Drawing.Size(75, 17);
            this.Advance_radio.TabIndex = 135;
            this.Advance_radio.TabStop = true;
            this.Advance_radio.Text = "Advance";
            this.Advance_radio.UseVisualStyleBackColor = true;
            this.Advance_radio.CheckedChanged += new System.EventHandler(this.Advance_radio_CheckedChanged);
            // 
            // SaveBtn
            // 
            this.SaveBtn.BackColor = System.Drawing.Color.MediumPurple;
            this.SaveBtn.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveBtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.SaveBtn.Image = ((System.Drawing.Image)(resources.GetObject("SaveBtn.Image")));
            this.SaveBtn.Location = new System.Drawing.Point(499, 412);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(130, 49);
            this.SaveBtn.TabIndex = 13;
            this.SaveBtn.Text = "Save Order";
            this.SaveBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.SaveBtn.UseVisualStyleBackColor = false;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.MenuText;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Image = global::Bakery_Management_System.Properties.Resources.Payment_History_30px;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(912, 411);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(124, 50);
            this.button4.TabIndex = 119;
            this.button4.Text = "Payment";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // radio_table
            // 
            this.radio_table.AutoSize = true;
            this.radio_table.Location = new System.Drawing.Point(158, 48);
            this.radio_table.Name = "radio_table";
            this.radio_table.Size = new System.Drawing.Size(57, 17);
            this.radio_table.TabIndex = 134;
            this.radio_table.Text = "Table";
            this.radio_table.UseVisualStyleBackColor = true;
            this.radio_table.CheckedChanged += new System.EventHandler(this.radio_table_CheckedChanged);
            // 
            // radio_take
            // 
            this.radio_take.AutoSize = true;
            this.radio_take.Checked = true;
            this.radio_take.Location = new System.Drawing.Point(93, 48);
            this.radio_take.Name = "radio_take";
            this.radio_take.Size = new System.Drawing.Size(54, 17);
            this.radio_take.TabIndex = 133;
            this.radio_take.TabStop = true;
            this.radio_take.Text = "Take";
            this.radio_take.UseVisualStyleBackColor = true;
            this.radio_take.CheckedChanged += new System.EventHandler(this.radio_take_CheckedChanged);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.SystemColors.WindowText;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(1181, 8);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(34, 32);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 119;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Visible = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // menulbl
            // 
            this.menulbl.AutoSize = true;
            this.menulbl.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.menulbl.Font = new System.Drawing.Font("Bahnschrift Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menulbl.ForeColor = System.Drawing.Color.White;
            this.menulbl.Location = new System.Drawing.Point(1120, 14);
            this.menulbl.Name = "menulbl";
            this.menulbl.Size = new System.Drawing.Size(57, 23);
            this.menulbl.TabIndex = 120;
            this.menulbl.Text = "Menu";
            this.menulbl.Click += new System.EventHandler(this.menulbl_Click);
            // 
            // Sidepanel
            // 
            this.Sidepanel.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.Sidepanel.Controls.Add(this.button2);
            this.Sidepanel.Controls.Add(this.label2);
            this.Sidepanel.Controls.Add(this.listBox3);
            this.Sidepanel.Controls.Add(this.listBox2);
            this.Sidepanel.Controls.Add(this.label33);
            this.Sidepanel.Controls.Add(this.listBox1);
            this.Sidepanel.Controls.Add(this.label20);
            this.Sidepanel.Location = new System.Drawing.Point(857, 46);
            this.Sidepanel.Name = "Sidepanel";
            this.Sidepanel.Size = new System.Drawing.Size(358, 429);
            this.Sidepanel.TabIndex = 132;
            this.Sidepanel.Visible = false;
            this.Sidepanel.Click += new System.EventHandler(this.Sidepanel_Click);
            this.Sidepanel.Paint += new System.Windows.Forms.PaintEventHandler(this.Sidepanel_Paint);
            this.Sidepanel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Sidepanel_MouseClick);
            this.Sidepanel.MouseHover += new System.EventHandler(this.Sidepanel_MouseHover);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Red;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button2.Location = new System.Drawing.Point(142, 311);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(118, 36);
            this.button2.TabIndex = 138;
            this.button2.Text = "Reset";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_2);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(54, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 20);
            this.label2.TabIndex = 119;
            this.label2.Text = "Delivery";
            // 
            // listBox3
            // 
            this.listBox3.FormattingEnabled = true;
            this.listBox3.Location = new System.Drawing.Point(46, 76);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(77, 186);
            this.listBox3.TabIndex = 118;
            this.listBox3.SelectedIndexChanged += new System.EventHandler(this.listBox3_SelectedIndexChanged);
            this.listBox3.DoubleClick += new System.EventHandler(this.listBox3_DoubleClick);
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(248, 76);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(76, 186);
            this.listBox2.TabIndex = 116;
            this.listBox2.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
            this.listBox2.DoubleClick += new System.EventHandler(this.listBox2_DoubleClick);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(259, 50);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 20);
            this.label33.TabIndex = 117;
            this.label33.Text = "Tables";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(154, 76);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(77, 186);
            this.listBox1.TabIndex = 98;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            this.listBox1.DoubleClick += new System.EventHandler(this.listBox1_DoubleClick);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(157, 50);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 20);
            this.label20.TabIndex = 99;
            this.label20.Text = "Advance";
            // 
            // ProductsGridView
            // 
            this.ProductsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProductsGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductIDColumn,
            this.ProductNameColumn,
            this.ProductPriceColumn,
            this.ProductQuantityColumn,
            this.TotalPriceColumn,
            this.Type,
            this.DeleteColumn,
            this.Increase,
            this.Decrease});
            this.ProductsGridView.Location = new System.Drawing.Point(558, 53);
            this.ProductsGridView.Name = "ProductsGridView";
            this.ProductsGridView.Size = new System.Drawing.Size(653, 308);
            this.ProductsGridView.TabIndex = 131;
            this.ProductsGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ProductsGridView_CellContentClick);
            // 
            // ProductIDColumn
            // 
            this.ProductIDColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ProductIDColumn.FillWeight = 40F;
            this.ProductIDColumn.HeaderText = "ID";
            this.ProductIDColumn.Name = "ProductIDColumn";
            this.ProductIDColumn.Width = 40;
            // 
            // ProductNameColumn
            // 
            this.ProductNameColumn.FillWeight = 147.8663F;
            this.ProductNameColumn.HeaderText = "Product Name";
            this.ProductNameColumn.Name = "ProductNameColumn";
            this.ProductNameColumn.Width = 190;
            // 
            // ProductPriceColumn
            // 
            this.ProductPriceColumn.FillWeight = 45.43843F;
            this.ProductPriceColumn.HeaderText = "Price";
            this.ProductPriceColumn.Name = "ProductPriceColumn";
            this.ProductPriceColumn.Width = 50;
            // 
            // ProductQuantityColumn
            // 
            this.ProductQuantityColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ProductQuantityColumn.HeaderText = "Quantity";
            this.ProductQuantityColumn.Name = "ProductQuantityColumn";
            this.ProductQuantityColumn.Width = 60;
            // 
            // TotalPriceColumn
            // 
            this.TotalPriceColumn.FillWeight = 9.326853F;
            this.TotalPriceColumn.HeaderText = "Product Total";
            this.TotalPriceColumn.Name = "TotalPriceColumn";
            this.TotalPriceColumn.Width = 70;
            // 
            // Type
            // 
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            this.Type.Width = 50;
            // 
            // DeleteColumn
            // 
            this.DeleteColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.DeleteColumn.HeaderText = "Delete";
            this.DeleteColumn.Image = global::Bakery_Management_System.Properties.Resources.Delete_26px;
            this.DeleteColumn.Name = "DeleteColumn";
            this.DeleteColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DeleteColumn.Width = 45;
            // 
            // Increase
            // 
            this.Increase.HeaderText = "Add";
            this.Increase.Image = global::Bakery_Management_System.Properties.Resources.Plus_Math_20px;
            this.Increase.Name = "Increase";
            this.Increase.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Increase.Width = 45;
            // 
            // Decrease
            // 
            this.Decrease.HeaderText = "Subtract";
            this.Decrease.Image = global::Bakery_Management_System.Properties.Resources.Subtract_26px;
            this.Decrease.Name = "Decrease";
            this.Decrease.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Decrease.Width = 60;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(1181, 10);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(34, 30);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 118;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // ProductsFlowPanel
            // 
            this.ProductsFlowPanel.AutoScroll = true;
            this.ProductsFlowPanel.BackColor = System.Drawing.Color.White;
            this.ProductsFlowPanel.Location = new System.Drawing.Point(352, 79);
            this.ProductsFlowPanel.Name = "ProductsFlowPanel";
            this.ProductsFlowPanel.Size = new System.Drawing.Size(200, 327);
            this.ProductsFlowPanel.TabIndex = 130;
            // 
            // CategoriesFlowPanel
            // 
            this.CategoriesFlowPanel.AutoScroll = true;
            this.CategoriesFlowPanel.BackColor = System.Drawing.Color.White;
            this.CategoriesFlowPanel.Location = new System.Drawing.Point(10, 201);
            this.CategoriesFlowPanel.Name = "CategoriesFlowPanel";
            this.CategoriesFlowPanel.Size = new System.Drawing.Size(316, 262);
            this.CategoriesFlowPanel.TabIndex = 129;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(12, 157);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 21);
            this.label10.TabIndex = 128;
            this.label10.Text = "Phone #";
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(88, 156);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(186, 24);
            this.textBox4.TabIndex = 127;
            // 
            // txt_custname
            // 
            this.txt_custname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_custname.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_custname.Location = new System.Drawing.Point(89, 117);
            this.txt_custname.Name = "txt_custname";
            this.txt_custname.Size = new System.Drawing.Size(186, 24);
            this.txt_custname.TabIndex = 126;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(88, 9);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(198, 26);
            this.textBox1.TabIndex = 61;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txt_grand_total
            // 
            this.txt_grand_total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_grand_total.Enabled = false;
            this.txt_grand_total.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25743F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_grand_total.ForeColor = System.Drawing.Color.Red;
            this.txt_grand_total.Location = new System.Drawing.Point(796, 367);
            this.txt_grand_total.Name = "txt_grand_total";
            this.txt_grand_total.Size = new System.Drawing.Size(173, 29);
            this.txt_grand_total.TabIndex = 45;
            this.txt_grand_total.Text = "0";
            // 
            // lbl_custmoer
            // 
            this.lbl_custmoer.AutoSize = true;
            this.lbl_custmoer.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_custmoer.Location = new System.Drawing.Point(6, 109);
            this.lbl_custmoer.Name = "lbl_custmoer";
            this.lbl_custmoer.Size = new System.Drawing.Size(81, 40);
            this.lbl_custmoer.TabIndex = 125;
            this.lbl_custmoer.Text = "Customer \r\n  Name";
            // 
            // combo_waiter
            // 
            this.combo_waiter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combo_waiter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combo_waiter.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_waiter.FormattingEnabled = true;
            this.combo_waiter.Location = new System.Drawing.Point(88, 116);
            this.combo_waiter.Name = "combo_waiter";
            this.combo_waiter.Size = new System.Drawing.Size(188, 26);
            this.combo_waiter.TabIndex = 124;
            // 
            // lbl_waiter
            // 
            this.lbl_waiter.AutoSize = true;
            this.lbl_waiter.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_waiter.Location = new System.Drawing.Point(12, 121);
            this.lbl_waiter.Name = "lbl_waiter";
            this.lbl_waiter.Size = new System.Drawing.Size(60, 21);
            this.lbl_waiter.TabIndex = 123;
            this.lbl_waiter.Text = "Waiter";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 21);
            this.label1.TabIndex = 121;
            this.label1.Text = "Order";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(748, 11);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(133, 25);
            this.label21.TabIndex = 118;
            this.label21.Text = "Order Detail:";
            // 
            // dataGridView2
            // 
            this.dataGridView2.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(1123, 433);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(86, 30);
            this.dataGridView2.TabIndex = 114;
            this.dataGridView2.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(12, 79);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(51, 21);
            this.label24.TabIndex = 113;
            this.label24.Text = "Table";
            // 
            // combo_table
            // 
            this.combo_table.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combo_table.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combo_table.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_table.FormattingEnabled = true;
            this.combo_table.Location = new System.Drawing.Point(86, 74);
            this.combo_table.Name = "combo_table";
            this.combo_table.Size = new System.Drawing.Size(188, 26);
            this.combo_table.TabIndex = 67;
            this.combo_table.SelectedIndexChanged += new System.EventHandler(this.combo_table_SelectedIndexChanged);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.White;
            this.button10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ForeColor = System.Drawing.Color.Red;
            this.button10.Image = global::Bakery_Management_System.Properties.Resources.Cancel_30px;
            this.button10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button10.Location = new System.Drawing.Point(397, 14);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(101, 41);
            this.button10.TabIndex = 106;
            this.button10.Text = "Cancel";
            this.button10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Visible = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CalendarFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(909, 7);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(137, 33);
            this.dateTimePicker1.TabIndex = 63;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Teal;
            this.button7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.Location = new System.Drawing.Point(635, 413);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(145, 48);
            this.button7.TabIndex = 77;
            this.button7.Text = "Order Change";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.69307F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(1375, 209);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(101, 51);
            this.button1.TabIndex = 100;
            this.button1.Text = "Clear ";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Teal;
            this.button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Image = global::Bakery_Management_System.Properties.Resources.Reserve_30px;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(1071, 442);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(46, 21);
            this.button3.TabIndex = 76;
            this.button3.Text = "Advance Order";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 10);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(66, 21);
            this.label13.TabIndex = 62;
            this.label13.Text = "Order #";
            // 
            // btn_process
            // 
            this.btn_process.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_process.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_process.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_process.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btn_process.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_process.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_process.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_process.Image = global::Bakery_Management_System.Properties.Resources.Print_30px;
            this.btn_process.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_process.Location = new System.Drawing.Point(784, 411);
            this.btn_process.Name = "btn_process";
            this.btn_process.Size = new System.Drawing.Size(122, 50);
            this.btn_process.TabIndex = 74;
            this.btn_process.Text = "Print Bill";
            this.btn_process.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_process.UseVisualStyleBackColor = false;
            this.btn_process.Click += new System.EventHandler(this.btn_process_Click_1);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(662, 368);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 25);
            this.label6.TabIndex = 46;
            this.label6.Text = "Grand Total :";
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(3, 48);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1248, 513);
            this.tabControl1.TabIndex = 12;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewImageColumn1.HeaderText = "Delete";
            this.dataGridViewImageColumn1.Image = global::Bakery_Management_System.Properties.Resources.Delete_26px;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.Width = 45;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.HeaderText = "Add";
            this.dataGridViewImageColumn2.Image = global::Bakery_Management_System.Properties.Resources.Add_New_26px;
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn2.Width = 45;
            // 
            // dataGridViewImageColumn3
            // 
            this.dataGridViewImageColumn3.HeaderText = "Subtract";
            this.dataGridViewImageColumn3.Image = global::Bakery_Management_System.Properties.Resources.Reduce_26px;
            this.dataGridViewImageColumn3.Name = "dataGridViewImageColumn3";
            this.dataGridViewImageColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn3.Width = 60;
            // 
            // btn_search
            // 
            this.btn_search.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn_search.BackColor = System.Drawing.Color.SteelBlue;
            this.btn_search.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_search.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btn_search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.11881F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_search.ForeColor = System.Drawing.Color.White;
            this.btn_search.Image = global::Bakery_Management_System.Properties.Resources.Search_30px1;
            this.btn_search.Location = new System.Drawing.Point(44, 88);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(87, 39);
            this.btn_search.TabIndex = 97;
            this.btn_search.UseVisualStyleBackColor = false;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // txt_serach_batch
            // 
            this.txt_serach_batch.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txt_serach_batch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_serach_batch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(25)))), ((int)(((byte)(30)))));
            this.txt_serach_batch.Location = new System.Drawing.Point(8, 49);
            this.txt_serach_batch.Name = "txt_serach_batch";
            this.txt_serach_batch.Size = new System.Drawing.Size(156, 26);
            this.txt_serach_batch.TabIndex = 96;
            this.txt_serach_batch.TextChanged += new System.EventHandler(this.txt_serach_batch_TextChanged);
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(25)))), ((int)(((byte)(30)))));
            this.label14.Location = new System.Drawing.Point(42, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 21);
            this.label14.TabIndex = 95;
            // 
            // button9
            // 
            this.button9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button9.BackColor = System.Drawing.Color.SteelBlue;
            this.button9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.11881F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Image = global::Bakery_Management_System.Properties.Resources.Search_30px1;
            this.button9.Location = new System.Drawing.Point(44, 88);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(87, 39);
            this.button9.TabIndex = 97;
            this.button9.UseVisualStyleBackColor = false;
            // 
            // textBox3
            // 
            this.textBox3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(25)))), ((int)(((byte)(30)))));
            this.textBox3.Location = new System.Drawing.Point(7, 52);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(156, 26);
            this.textBox3.TabIndex = 96;
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(25)))), ((int)(((byte)(30)))));
            this.label19.Location = new System.Drawing.Point(42, 21);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(66, 21);
            this.label19.TabIndex = 95;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(500, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 25);
            this.label3.TabIndex = 181;
            this.label3.Text = "Address";
            // 
            // textAddress
            // 
            this.textAddress.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textAddress.Location = new System.Drawing.Point(586, 13);
            this.textAddress.Multiline = true;
            this.textAddress.Name = "textAddress";
            this.textAddress.Size = new System.Drawing.Size(194, 26);
            this.textAddress.TabIndex = 180;
            // 
            // Point_Sale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Name = "Point_Sale";
            this.Size = new System.Drawing.Size(1233, 552);
            this.Load += new System.EventHandler(this.Point_Sale_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.Sidepanel.ResumeLayout(false);
            this.Sidepanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProductsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn3;
        private System.Windows.Forms.TabPage tabPage1;
        public System.Windows.Forms.RadioButton radio_table;
        public System.Windows.Forms.RadioButton radio_take;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label menulbl;
        private System.Windows.Forms.Panel Sidepanel;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.DataGridView ProductsGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductIDColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductPriceColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductQuantityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalPriceColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewImageColumn DeleteColumn;
        private System.Windows.Forms.DataGridViewImageColumn Increase;
        private System.Windows.Forms.DataGridViewImageColumn Decrease;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.FlowLayoutPanel ProductsFlowPanel;
        private System.Windows.Forms.FlowLayoutPanel CategoriesFlowPanel;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox textBox4;
        public System.Windows.Forms.TextBox txt_custname;
        public System.Windows.Forms.TextBox textBox1;
        public System.Windows.Forms.TextBox txt_grand_total;
        private System.Windows.Forms.Label lbl_custmoer;
        private System.Windows.Forms.ComboBox combo_waiter;
        private System.Windows.Forms.Label lbl_waiter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label24;
        public System.Windows.Forms.ComboBox combo_table;
        private System.Windows.Forms.Button button10;
        public System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btn_process;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.TextBox txt_serach_batch;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.RadioButton Advance_radio;
        private System.Windows.Forms.Button SaveAdvance;
        private System.Windows.Forms.RadioButton Delivery_radio;
        private System.Windows.Forms.Button Delivery_btn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textAddress;
    }
}
