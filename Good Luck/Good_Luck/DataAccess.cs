﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Data;
using DAL;
namespace Bakery_Management_System
{
    class DataAccess
    {
        string ConnectionString1;
        DB_Connecctions db = new DB_Connecctions();
        public DataAccess()
        {
            ConnectionString1 = ConnectionString.ConString;
        }
        public Details RetreiveProductDetails(int ProductID)
        {
            Details ProductDetails = new Details();

            using (SqlConnection connection = new SqlConnection(ConnectionString1))
            {
                SqlCommand command = new SqlCommand("SELECT Deal_Name, Price, CatogryID, Deal_Detail,Type,FileName,FilePath FROM Deals where Deal_ID = '" + ProductID + "';", connection);
                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        ProductDetails.Name = reader.GetString(0);
                        ProductDetails.Price = reader.GetInt32(1);
                        int ProductCategoryID = reader.GetInt32(2);
                        ProductDetails.Category = ReturnCategoryName(ProductCategoryID);
                        ProductDetails.Description = reader.GetString(3);
                        //ProductDetails.Picture = (byte[])reader[4];
                        //ProductDetails.protype = reader.GetString(4)!=null ? reader.GetString(4):"0";
                        ProductDetails.FileName = reader.GetString(5);
                        ProductDetails.FilePath = reader.GetString(6);
                    }
                }
                reader.Close();

                return ProductDetails;
            }
        }
        public string ReturnCategoryName(int CategoryID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString1))
            {
                SqlCommand command = new SqlCommand("SELECT CategoryName FROM Categories where ID = '" + CategoryID + "';", connection);
                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                string CategoryName = string.Empty;

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        CategoryName = reader.GetString(0);
                    }
                }
                reader.Close();

                return CategoryName;
            }
        }



        internal DataTable getallCategory()
        {

            String command = "SELECT ID,CategoryName,CategoryDescription,Status,FileName,FilePath FROM Categories where Status=1";
            DB_Connecctions db = new DB_Connecctions();
            DataSet ds = db.Select(command);
            return ds.Tables[0];

        }

        internal DataTable AllDeals(int CategoryID)
        {
            String command = "SELECT Deal_ID,Deal_Name,Deal_Detail,Price,Type,CatogryID,Status,Recipe,FileName,FilePath FROM Deals where  CatogryID = '" + CategoryID + "' and  Status=1";
            DB_Connecctions db = new DB_Connecctions();
            DataSet ds = db.Select(command);
            return ds.Tables[0];
        }
    }
}
