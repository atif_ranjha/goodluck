﻿namespace Bakery_Management_System
{
    partial class POS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(POS));
            this.Advance_radio = new System.Windows.Forms.RadioButton();
            this.radio_table = new System.Windows.Forms.RadioButton();
            this.radio_take = new System.Windows.Forms.RadioButton();
            this.CategoriesFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_custname = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lbl_custmoer = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.combo_table = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Delivery_btn = new System.Windows.Forms.Button();
            this.SaveAdvance = new System.Windows.Forms.Button();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.menulbl = new System.Windows.Forms.Label();
            this.Sidepanel = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.label33 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label20 = new System.Windows.Forms.Label();
            this.ProductsGridView = new System.Windows.Forms.DataGridView();
            this.ProductIDColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductPriceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductQuantityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TotalPriceColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeleteColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.Increase = new System.Windows.Forms.DataGridViewImageColumn();
            this.Decrease = new System.Windows.Forms.DataGridViewImageColumn();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.txt_grand_total = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button7 = new System.Windows.Forms.Button();
            this.btn_process = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textAddress = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.ProductsFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.Delivery_radio = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.btnsrc2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textRecive = new System.Windows.Forms.TextBox();
            this.textReturn = new System.Windows.Forms.TextBox();
            this.Sidepanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProductsGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // Advance_radio
            // 
            this.Advance_radio.AutoSize = true;
            this.Advance_radio.Location = new System.Drawing.Point(854, 46);
            this.Advance_radio.Name = "Advance_radio";
            this.Advance_radio.Size = new System.Drawing.Size(68, 17);
            this.Advance_radio.TabIndex = 156;
            this.Advance_radio.Text = "Advance";
            this.Advance_radio.UseVisualStyleBackColor = true;
            this.Advance_radio.CheckedChanged += new System.EventHandler(this.Advance_radio_CheckedChanged);
            // 
            // radio_table
            // 
            this.radio_table.AutoSize = true;
            this.radio_table.Location = new System.Drawing.Point(796, 47);
            this.radio_table.Name = "radio_table";
            this.radio_table.Size = new System.Drawing.Size(52, 17);
            this.radio_table.TabIndex = 155;
            this.radio_table.Text = "Table";
            this.radio_table.UseVisualStyleBackColor = true;
            this.radio_table.CheckedChanged += new System.EventHandler(this.radio_table_CheckedChanged);
            // 
            // radio_take
            // 
            this.radio_take.AutoSize = true;
            this.radio_take.Checked = true;
            this.radio_take.Location = new System.Drawing.Point(740, 47);
            this.radio_take.Name = "radio_take";
            this.radio_take.Size = new System.Drawing.Size(50, 17);
            this.radio_take.TabIndex = 154;
            this.radio_take.TabStop = true;
            this.radio_take.Text = "Take";
            this.radio_take.UseVisualStyleBackColor = true;
            this.radio_take.CheckedChanged += new System.EventHandler(this.radio_take_CheckedChanged);
            // 
            // CategoriesFlowPanel
            // 
            this.CategoriesFlowPanel.AutoScroll = true;
            this.CategoriesFlowPanel.BackColor = System.Drawing.Color.PowderBlue;
            this.CategoriesFlowPanel.Location = new System.Drawing.Point(545, 353);
            this.CategoriesFlowPanel.Name = "CategoriesFlowPanel";
            this.CategoriesFlowPanel.Size = new System.Drawing.Size(798, 371);
            this.CategoriesFlowPanel.TabIndex = 151;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(655, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 25);
            this.label10.TabIndex = 150;
            this.label10.Text = "Phone #";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // txt_custname
            // 
            this.txt_custname.BackColor = System.Drawing.Color.WhiteSmoke;
            this.txt_custname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_custname.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_custname.Location = new System.Drawing.Point(460, 10);
            this.txt_custname.Name = "txt_custname";
            this.txt_custname.Size = new System.Drawing.Size(194, 24);
            this.txt_custname.TabIndex = 148;
            this.txt_custname.TextChanged += new System.EventHandler(this.txt_custname_TextChanged);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(119, 9);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(188, 26);
            this.textBox1.TabIndex = 137;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lbl_custmoer
            // 
            this.lbl_custmoer.AutoSize = true;
            this.lbl_custmoer.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_custmoer.Location = new System.Drawing.Point(308, 10);
            this.lbl_custmoer.Name = "lbl_custmoer";
            this.lbl_custmoer.Size = new System.Drawing.Size(152, 25);
            this.lbl_custmoer.TabIndex = 147;
            this.lbl_custmoer.Text = "Customer Name";
            this.lbl_custmoer.Click += new System.EventHandler(this.lbl_custmoer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(676, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 25);
            this.label1.TabIndex = 144;
            this.label1.Text = "Order";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(41, 44);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(56, 25);
            this.label24.TabIndex = 141;
            this.label24.Text = "Table";
            // 
            // combo_table
            // 
            this.combo_table.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.combo_table.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.combo_table.BackColor = System.Drawing.Color.WhiteSmoke;
            this.combo_table.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_table.FormattingEnabled = true;
            this.combo_table.Location = new System.Drawing.Point(119, 43);
            this.combo_table.Name = "combo_table";
            this.combo_table.Size = new System.Drawing.Size(188, 26);
            this.combo_table.TabIndex = 139;
            this.combo_table.SelectedIndexChanged += new System.EventHandler(this.combo_table_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(20, 11);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 25);
            this.label13.TabIndex = 138;
            this.label13.Text = "Order #";
            // 
            // Delivery_btn
            // 
            this.Delivery_btn.BackColor = System.Drawing.Color.RoyalBlue;
            this.Delivery_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Delivery_btn.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Delivery_btn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Delivery_btn.Image = ((System.Drawing.Image)(resources.GetObject("Delivery_btn.Image")));
            this.Delivery_btn.Location = new System.Drawing.Point(743, 73);
            this.Delivery_btn.Name = "Delivery_btn";
            this.Delivery_btn.Size = new System.Drawing.Size(126, 52);
            this.Delivery_btn.TabIndex = 176;
            this.Delivery_btn.Text = "Delivery";
            this.Delivery_btn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Delivery_btn.UseVisualStyleBackColor = false;
            this.Delivery_btn.Click += new System.EventHandler(this.Delivery_btn_Click);
            // 
            // SaveAdvance
            // 
            this.SaveAdvance.BackColor = System.Drawing.Color.LightSeaGreen;
            this.SaveAdvance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveAdvance.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveAdvance.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.SaveAdvance.Image = ((System.Drawing.Image)(resources.GetObject("SaveAdvance.Image")));
            this.SaveAdvance.Location = new System.Drawing.Point(611, 73);
            this.SaveAdvance.Name = "SaveAdvance";
            this.SaveAdvance.Size = new System.Drawing.Size(126, 52);
            this.SaveAdvance.TabIndex = 175;
            this.SaveAdvance.Text = "Advance";
            this.SaveAdvance.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.SaveAdvance.UseVisualStyleBackColor = false;
            this.SaveAdvance.Click += new System.EventHandler(this.SaveAdvance_Click);
            // 
            // SaveBtn
            // 
            this.SaveBtn.BackColor = System.Drawing.Color.MediumPurple;
            this.SaveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveBtn.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveBtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.SaveBtn.Image = ((System.Drawing.Image)(resources.GetObject("SaveBtn.Image")));
            this.SaveBtn.Location = new System.Drawing.Point(611, 74);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(126, 52);
            this.SaveBtn.TabIndex = 158;
            this.SaveBtn.Text = "Save Order";
            this.SaveBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.SaveBtn.UseVisualStyleBackColor = false;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.DarkGreen;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(1007, 73);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(126, 52);
            this.button4.TabIndex = 170;
            this.button4.Text = "Payment";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // menulbl
            // 
            this.menulbl.AutoSize = true;
            this.menulbl.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.menulbl.Font = new System.Drawing.Font("Bahnschrift Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menulbl.ForeColor = System.Drawing.Color.White;
            this.menulbl.Location = new System.Drawing.Point(14, 75);
            this.menulbl.Name = "menulbl";
            this.menulbl.Size = new System.Drawing.Size(141, 23);
            this.menulbl.TabIndex = 171;
            this.menulbl.Text = "Pending Orders";
            // 
            // Sidepanel
            // 
            this.Sidepanel.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.Sidepanel.Controls.Add(this.button6);
            this.Sidepanel.Controls.Add(this.button3);
            this.Sidepanel.Controls.Add(this.label2);
            this.Sidepanel.Controls.Add(this.listBox3);
            this.Sidepanel.Controls.Add(this.listBox2);
            this.Sidepanel.Controls.Add(this.label33);
            this.Sidepanel.Controls.Add(this.listBox1);
            this.Sidepanel.Controls.Add(this.label20);
            this.Sidepanel.Location = new System.Drawing.Point(12, 101);
            this.Sidepanel.Name = "Sidepanel";
            this.Sidepanel.Size = new System.Drawing.Size(358, 393);
            this.Sidepanel.TabIndex = 174;
            this.Sidepanel.Visible = false;
            this.Sidepanel.Paint += new System.Windows.Forms.PaintEventHandler(this.Sidepanel_Paint);
            this.Sidepanel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Sidepanel_MouseClick);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.SlateGray;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button6.Location = new System.Drawing.Point(184, 332);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(107, 36);
            this.button6.TabIndex = 140;
            this.button6.Text = "Refresh";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Red;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button3.Location = new System.Drawing.Point(70, 332);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(107, 36);
            this.button3.TabIndex = 139;
            this.button3.Text = "Reset";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(54, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 20);
            this.label2.TabIndex = 119;
            this.label2.Text = "Delivery";
            // 
            // listBox3
            // 
            this.listBox3.FormattingEnabled = true;
            this.listBox3.Location = new System.Drawing.Point(47, 76);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(77, 186);
            this.listBox3.TabIndex = 118;
            this.listBox3.SelectedIndexChanged += new System.EventHandler(this.listBox3_SelectedIndexChanged);
            this.listBox3.DoubleClick += new System.EventHandler(this.listBox3_DoubleClick);
            this.listBox3.MouseLeave += new System.EventHandler(this.listBox3_MouseLeave);
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(248, 76);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(76, 186);
            this.listBox2.TabIndex = 116;
            this.listBox2.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
            this.listBox2.DoubleClick += new System.EventHandler(this.listBox2_DoubleClick);
            this.listBox2.MouseLeave += new System.EventHandler(this.listBox2_MouseLeave);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(259, 50);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 20);
            this.label33.TabIndex = 117;
            this.label33.Text = "Tables";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(154, 76);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(77, 186);
            this.listBox1.TabIndex = 98;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            this.listBox1.DoubleClick += new System.EventHandler(this.listBox1_DoubleClick);
            this.listBox1.MouseLeave += new System.EventHandler(this.listBox1_MouseLeave);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(157, 50);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 20);
            this.label20.TabIndex = 99;
            this.label20.Text = "Advance";
            // 
            // ProductsGridView
            // 
            this.ProductsGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ProductsGridView.BackgroundColor = System.Drawing.Color.PowderBlue;
            this.ProductsGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProductsGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductIDColumn,
            this.ProductNameColumn,
            this.ProductPriceColumn,
            this.ProductQuantityColumn,
            this.TotalPriceColumn,
            this.Type,
            this.DeleteColumn,
            this.Increase,
            this.Decrease});
            this.ProductsGridView.Location = new System.Drawing.Point(12, 111);
            this.ProductsGridView.Name = "ProductsGridView";
            this.ProductsGridView.Size = new System.Drawing.Size(520, 517);
            this.ProductsGridView.TabIndex = 173;
            this.ProductsGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ProductsGridView_CellContentClick);
            this.ProductsGridView.Click += new System.EventHandler(this.ProductsGridView_Click);
            // 
            // ProductIDColumn
            // 
            this.ProductIDColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ProductIDColumn.FillWeight = 40F;
            this.ProductIDColumn.HeaderText = "ID";
            this.ProductIDColumn.Name = "ProductIDColumn";
            this.ProductIDColumn.Width = 40;
            // 
            // ProductNameColumn
            // 
            this.ProductNameColumn.FillWeight = 103.1895F;
            this.ProductNameColumn.HeaderText = "Product Name";
            this.ProductNameColumn.Name = "ProductNameColumn";
            // 
            // ProductPriceColumn
            // 
            this.ProductPriceColumn.FillWeight = 101.0558F;
            this.ProductPriceColumn.HeaderText = "Price";
            this.ProductPriceColumn.Name = "ProductPriceColumn";
            // 
            // ProductQuantityColumn
            // 
            this.ProductQuantityColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ProductQuantityColumn.HeaderText = "Quantity";
            this.ProductQuantityColumn.Name = "ProductQuantityColumn";
            this.ProductQuantityColumn.Width = 20;
            // 
            // TotalPriceColumn
            // 
            this.TotalPriceColumn.FillWeight = 136.5847F;
            this.TotalPriceColumn.HeaderText = "Product Total";
            this.TotalPriceColumn.Name = "TotalPriceColumn";
            // 
            // Type
            // 
            this.Type.FillWeight = 22.23028F;
            this.Type.HeaderText = "Type";
            this.Type.Name = "Type";
            // 
            // DeleteColumn
            // 
            this.DeleteColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.DeleteColumn.HeaderText = "Delete";
            this.DeleteColumn.Image = global::Bakery_Management_System.Properties.Resources.Delete_26px;
            this.DeleteColumn.Name = "DeleteColumn";
            this.DeleteColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DeleteColumn.Width = 45;
            // 
            // Increase
            // 
            this.Increase.FillWeight = 69.78568F;
            this.Increase.HeaderText = "Add";
            this.Increase.Image = global::Bakery_Management_System.Properties.Resources.Plus_Math_20px;
            this.Increase.Name = "Increase";
            this.Increase.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Decrease
            // 
            this.Decrease.FillWeight = 69.78568F;
            this.Decrease.HeaderText = "Subtract";
            this.Decrease.Image = global::Bakery_Management_System.Properties.Resources.Subtract_26px;
            this.Decrease.Name = "Decrease";
            this.Decrease.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(155, 75);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(34, 30);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 168;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // txt_grand_total
            // 
            this.txt_grand_total.BackColor = System.Drawing.Color.PowderBlue;
            this.txt_grand_total.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_grand_total.Enabled = false;
            this.txt_grand_total.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25743F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_grand_total.ForeColor = System.Drawing.Color.Red;
            this.txt_grand_total.Location = new System.Drawing.Point(180, 636);
            this.txt_grand_total.Multiline = true;
            this.txt_grand_total.Name = "txt_grand_total";
            this.txt_grand_total.Size = new System.Drawing.Size(55, 25);
            this.txt_grand_total.TabIndex = 159;
            this.txt_grand_total.Text = "0";
            this.txt_grand_total.TextChanged += new System.EventHandler(this.txt_grand_total_TextChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI Black", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(1073, 7);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(133, 25);
            this.label21.TabIndex = 167;
            this.label21.Text = "Order Detail:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CalendarFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(1207, 5);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(137, 33);
            this.dateTimePicker1.TabIndex = 161;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Teal;
            this.button7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.Location = new System.Drawing.Point(875, 73);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(126, 53);
            this.button7.TabIndex = 164;
            this.button7.Text = "Order Change";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // btn_process
            // 
            this.btn_process.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_process.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_process.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_process.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btn_process.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_process.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_process.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_process.Image = ((System.Drawing.Image)(resources.GetObject("btn_process.Image")));
            this.btn_process.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_process.Location = new System.Drawing.Point(743, 73);
            this.btn_process.Name = "btn_process";
            this.btn_process.Size = new System.Drawing.Size(126, 52);
            this.btn_process.TabIndex = 162;
            this.btn_process.Text = "Print Bill";
            this.btn_process.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_process.UseVisualStyleBackColor = false;
            this.btn_process.Click += new System.EventHandler(this.btn_process_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(26, 631);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 30);
            this.label6.TabIndex = 160;
            this.label6.Text = "Grand Total :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(119, 9);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(123, 25);
            this.label18.TabIndex = 177;
            this.label18.Text = "Printer name";
            this.label18.Visible = false;
            // 
            // textAddress
            // 
            this.textAddress.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textAddress.Location = new System.Drawing.Point(460, 41);
            this.textAddress.Name = "textAddress";
            this.textAddress.Size = new System.Drawing.Size(194, 24);
            this.textAddress.TabIndex = 178;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(380, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 25);
            this.label3.TabIndex = 179;
            this.label3.Text = "Address";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.SkyBlue;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Image = global::Bakery_Management_System.Properties.Resources.Cancel_30px;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(1007, 73);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 52);
            this.button1.TabIndex = 180;
            this.button1.Text = "Cancel";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // ProductsFlowPanel
            // 
            this.ProductsFlowPanel.AutoScroll = true;
            this.ProductsFlowPanel.BackColor = System.Drawing.Color.PowderBlue;
            this.ProductsFlowPanel.Location = new System.Drawing.Point(545, 131);
            this.ProductsFlowPanel.Name = "ProductsFlowPanel";
            this.ProductsFlowPanel.Size = new System.Drawing.Size(798, 218);
            this.ProductsFlowPanel.TabIndex = 181;
            // 
            // Delivery_radio
            // 
            this.Delivery_radio.AutoSize = true;
            this.Delivery_radio.Location = new System.Drawing.Point(928, 46);
            this.Delivery_radio.Name = "Delivery_radio";
            this.Delivery_radio.Size = new System.Drawing.Size(63, 17);
            this.Delivery_radio.TabIndex = 182;
            this.Delivery_radio.TabStop = true;
            this.Delivery_radio.Text = "Delivery";
            this.Delivery_radio.UseVisualStyleBackColor = true;
            this.Delivery_radio.CheckedChanged += new System.EventHandler(this.Delivery_radio_CheckedChanged);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.RoyalBlue;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(1139, 73);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(126, 52);
            this.button2.TabIndex = 183;
            this.button2.Text = "Add Bill";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(740, 9);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(194, 24);
            this.textBox4.TabIndex = 149;
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            this.textBox4.Enter += new System.EventHandler(this.textBox4_Enter);
            this.textBox4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox4_KeyPress);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.LightSlateGray;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button5.Location = new System.Drawing.Point(1007, 30);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(107, 39);
            this.button5.TabIndex = 184;
            this.button5.Text = "Refresh";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnsrc2
            // 
            this.btnsrc2.FlatAppearance.BorderSize = 0;
            this.btnsrc2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnsrc2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnsrc2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsrc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsrc2.Image = global::Bakery_Management_System.Properties.Resources.Search_30px;
            this.btnsrc2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsrc2.Location = new System.Drawing.Point(939, 7);
            this.btnsrc2.Name = "btnsrc2";
            this.btnsrc2.Size = new System.Drawing.Size(48, 35);
            this.btnsrc2.TabIndex = 185;
            this.btnsrc2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnsrc2.UseVisualStyleBackColor = true;
            this.btnsrc2.Click += new System.EventHandler(this.btnsrc2_Click_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(270, 631);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(148, 30);
            this.label4.TabIndex = 186;
            this.label4.Text = "Recived Total:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 661);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(145, 30);
            this.label5.TabIndex = 187;
            this.label5.Text = "Return Total :";
            // 
            // textRecive
            // 
            this.textRecive.BackColor = System.Drawing.Color.White;
            this.textRecive.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textRecive.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25743F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textRecive.ForeColor = System.Drawing.Color.Red;
            this.textRecive.Location = new System.Drawing.Point(415, 636);
            this.textRecive.Multiline = true;
            this.textRecive.Name = "textRecive";
            this.textRecive.Size = new System.Drawing.Size(101, 25);
            this.textRecive.TabIndex = 188;
            this.textRecive.TextChanged += new System.EventHandler(this.textRecive_TextChanged);
            // 
            // textReturn
            // 
            this.textReturn.BackColor = System.Drawing.Color.PowderBlue;
            this.textReturn.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textReturn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25743F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textReturn.ForeColor = System.Drawing.Color.Red;
            this.textReturn.Location = new System.Drawing.Point(180, 667);
            this.textReturn.Multiline = true;
            this.textReturn.Name = "textReturn";
            this.textReturn.Size = new System.Drawing.Size(55, 25);
            this.textReturn.TabIndex = 189;
            // 
            // POS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1280, 736);
            this.Controls.Add(this.textReturn);
            this.Controls.Add(this.textRecive);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Delivery_btn);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.btnsrc2);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.Sidepanel);
            this.Controls.Add(this.ProductsGridView);
            this.Controls.Add(this.menulbl);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Delivery_radio);
            this.Controls.Add(this.ProductsFlowPanel);
            this.Controls.Add(this.btn_process);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textAddress);
            this.Controls.Add(this.SaveAdvance);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txt_grand_total);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Advance_radio);
            this.Controls.Add(this.radio_table);
            this.Controls.Add(this.radio_take);
            this.Controls.Add(this.CategoriesFlowPanel);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.txt_custname);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lbl_custmoer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.combo_table);
            this.Controls.Add(this.label13);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "POS";
            this.Text = "POS";
            this.Load += new System.EventHandler(this.POS_Load);
            this.Sidepanel.ResumeLayout(false);
            this.Sidepanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProductsGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton Advance_radio;
        public System.Windows.Forms.RadioButton radio_table;
        public System.Windows.Forms.RadioButton radio_take;
        private System.Windows.Forms.FlowLayoutPanel CategoriesFlowPanel;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txt_custname;
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lbl_custmoer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label24;
        public System.Windows.Forms.ComboBox combo_table;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button Delivery_btn;
        private System.Windows.Forms.Button SaveAdvance;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label menulbl;
        private System.Windows.Forms.Panel Sidepanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.DataGridView ProductsGridView;
        private System.Windows.Forms.PictureBox pictureBox4;
        public System.Windows.Forms.TextBox txt_grand_total;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button btn_process;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textAddress;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.FlowLayoutPanel ProductsFlowPanel;
        private System.Windows.Forms.RadioButton Delivery_radio;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductIDColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductPriceColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductQuantityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalPriceColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type;
        private System.Windows.Forms.DataGridViewImageColumn DeleteColumn;
        private System.Windows.Forms.DataGridViewImageColumn Increase;
        private System.Windows.Forms.DataGridViewImageColumn Decrease;
        private System.Windows.Forms.Button button3;
        public System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button btnsrc2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox textRecive;
        public System.Windows.Forms.TextBox textReturn;
    }
}