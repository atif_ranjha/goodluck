﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
namespace Bakery_Management_System
{
    public partial class Rpt_Receipt_form : Form
    {
        public Rpt_Receipt_form()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection(@"Data Source=DELL-INSPIRON;Initial Catalog=QFC;Integrated Security=True");
        private void Rpt_Receipt_form_Load(object sender, EventArgs e)
        {
            DB_Connecctions db = new DB_Connecctions();
            con.Open();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from sale where Batch_ID=42";
            cmd.ExecuteNonQuery();
            Bakery_Sale_Receipt_DataSet ds = new Bakery_Sale_Receipt_DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds, "Sale");
            Rpt_Receipt myreport = new Rpt_Receipt();
            myreport.SetDataSource(ds);
            //myreport.PrintOptions.PrinterName = "Microsoft print to PDF";
            //myreport.PrintToPrinter(1, false, 0, 0);
            crystalReportViewer1.ReportSource = myreport;
            con.Close();
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

        }
    }
}
