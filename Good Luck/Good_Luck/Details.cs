﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bakery_Management_System
{
    class Details
    {
        public int ID;
        public string Name;
        public byte[] Picture;
        public decimal Price;
        public int Quantity;
        public decimal Total;
        public string Category;
        public string Description;
        public int SaleID;
        public DateTime SaleTime;
        public int SalesmanID;
        public string protype;
        public string FileName;
        public string FilePath;
    }
}
