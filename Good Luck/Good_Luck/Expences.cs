﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bakery_Management_System
{
    public partial class Expences : UserControl
    {
        public Expences()
        {
            InitializeComponent();
        }
        // *****+++++++++++++++ DATABASE CONNECTION CLASS GLOBAL ++++++++++++*******//
        DB_Connecctions db = new DB_Connecctions();
        string con;
        int a;
        // *****+++++++++++++++ TO REFRESH GRID VIEW ++++++++++++*******//
        public void refre()
        {
            DataSet ds = db.Select("select ID as 'Serial No',Detail,Amount,Date from Expences where Status=1");
            dataGridView1.DataSource = ds.Tables["tbl1"].DefaultView;
        }
        public void refree2()
        {
            ///***** REOCRD //***++++
            DataSet ds = db.Select("Select ID,Detail,Amount,Date from expences where status=1");
            dataGridView2.DataSource = ds.Tables["tbl1"].DefaultView;
            DataSet cou = db.Select("select count(ID) from expences where status=1");
            txt_totl_sales.Text = cou.Tables["tbl1"].Rows[0][0].ToString();
            DataSet cash = db.Select("select SUM(Amount) from expences where status=1");
            txt_tot_cash.Text = cash.Tables["tbl1"].Rows[0][0].ToString();
        }
        // *****+++++++++++++++ TO GET ITEM CODE ++++++++++++*******//
        public void get_id()
        {
            DataSet ds = db.Select("select ID from Expences");
            if (ds != null && ds.Tables["tbl1"].Rows.Count > 0)
            {
                int a = ds.Tables["tbl1"].Rows.Count;
                txt_item_Code.Text = (Convert.ToInt32(ds.Tables["tbl1"].Rows[a - 1][0]) + 1).ToString();
            }
            else
            {
                txt_item_Code.Text = "1";
            }
        }
        public void clear()
        {
            txt_discription.Clear();
            txt_item_name.Clear();
        }
        private void Expences_Load(object sender, EventArgs e)
        {
            get_id();
            refre();
            btn_save.Enabled = true;
            btn_update.Enabled = false;
            btn_delete.Enabled = false;
            dateTimePicker1.Value = System.DateTime.Today;//*** TO GET TODAY DATE
            //** TO HIDE CONTROLS //** ++++
            dateTimePicker2.Visible = false;
            combo_month.Visible = false;
            combo_year.Visible = false;
            btn_ser.Visible = false;
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            db.ExecuteQuery("Insert into expences (ID,Detail,Amount,Date,Status) values('"+txt_item_Code.Text+"','"+txt_item_name.Text+"','"+txt_discription.Text+"','"+dateTimePicker1.Value+"',1)");
            MessageBox.Show("Saved.", "Expences", MessageBoxButtons.OK, MessageBoxIcon.Information);
            clear();
            get_id();
            refre();
        }

        private void btn_update_Click(object sender, EventArgs e)
        {
            db.ExecuteQuery("Update Expences Set Status=0 where ID='"+txt_item_Code.Text+"'");
            MessageBox.Show("Deleted.", "Expences", MessageBoxButtons.OK, MessageBoxIcon.Information);
            clear();
            get_id();
            refre();
            btn_save.Enabled = true;
            btn_delete.Enabled = false;
            btn_update.Enabled = false;
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            clear();
            get_id();
            btn_save.Enabled = true;
            btn_delete.Enabled = false;
            btn_update.Enabled = false;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.dataGridView1.Rows[e.RowIndex];
                txt_item_Code.Text = row.Cells[0].Value.ToString();
                txt_item_name.Text = row.Cells[1].Value.ToString();
                txt_discription.Text = row.Cells[2].Value.ToString();
                dateTimePicker1.Text = row.Cells[3].Value.ToString();
            }
            btn_save.Enabled = false;
            btn_delete.Enabled = true;
            btn_update.Enabled = true;
        }
        //********* ALL EXPENCES //////
        private void btn_allSale_Click(object sender, EventArgs e)
        {
            //** TO HIDE CONTROLS //** ++++
            dateTimePicker1.Visible = false;
            combo_month.Visible = false;
            combo_year.Visible = false;
            btn_ser.Visible = false;
            ///***** REOCRD //***++++
            DataSet ds = db.Select("Select ID,Detail,Amount,Date from expences where status=1");
            dataGridView2.DataSource = ds.Tables["tbl1"].DefaultView;
            DataSet cou = db.Select("select count(ID) from expences where status=1");
            txt_totl_sales.Text = cou.Tables["tbl1"].Rows[0][0].ToString();
            DataSet cash = db.Select("select SUM(Amount) from expences where status=1");
            txt_tot_cash.Text = cash.Tables["tbl1"].Rows[0][0].ToString();
        }

        private void btn_daily_Click(object sender, EventArgs e)
        {
            //** TO HIDE CONTROLS //** ++++
            dateTimePicker2.Visible = true;
            combo_month.Visible = false;
            combo_year.Visible = false;
            btn_ser.Visible = false;
            refree2();
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            dateTimePicker2.Visible = true;
            DataSet ds = db.Select("Select ID,Detail,Amount,Date from expences  where date='"+dateTimePicker2.Value+"' and status=1");
            dataGridView2.DataSource = ds.Tables["tbl1"].DefaultView;
            DataSet cou = db.Select("select count(ID) from expences where date='" + dateTimePicker2.Value + "' and status=1");
            txt_totl_sales.Text = cou.Tables["tbl1"].Rows[0][0].ToString();
            DataSet cash = db.Select("select SUM(Amount) from expences where date='" + dateTimePicker2.Value + "' and status=1");
            txt_tot_cash.Text = cash.Tables["tbl1"].Rows[0][0].ToString();
        }

        private void btn_monthly_Click(object sender, EventArgs e)
        {
            //** TO HIDE CONTROLS //** ++++
            dateTimePicker2.Visible = false;
            combo_month.Visible = true;
            combo_year.Visible = true;
            btn_ser.Visible = false;
        }
        //**** FUNCTION CODE FOR MONTHLY SEARCH ///++++++
        public void month_ser()
        {
            DataSet ds = db.Select("Select ID,Detail,Amount,Date from expences where MONTH(date)='" + a + "' and Year(date)='" + combo_year.Text + "'");
            dataGridView2.DataSource = ds.Tables["tbl1"].DefaultView;
            DataSet cou = db.Select("select count(ID) from expences where MONTH(date)='" + a + "' and Year(date)='" + combo_year.Text + "'");
            txt_totl_sales.Text = cou.Tables["tbl1"].Rows[0][0].ToString();
            DataSet cash = db.Select("select SUM(amount) from expences where MONTH(date)='" + a + "' and Year(date)='" + combo_year.Text + "'");
            txt_tot_cash.Text = cash.Tables["tbl1"].Rows[0][0].ToString();
        }

        private void btn_annual_Click(object sender, EventArgs e)
        {
            //** TO HIDE CONTROLS //** ++++
            dateTimePicker2.Visible = false;
            combo_month.Visible = false;
            combo_year.Visible = true;
            btn_ser.Visible = true;
        }

        private void btn_ser_Click(object sender, EventArgs e)
        {
            DataSet ds = db.Select("Select ID,Detail,Amount,Date from expences where Year(date)='" + combo_year.Text + "'");
            dataGridView2.DataSource = ds.Tables["tbl1"].DefaultView;
            DataSet cou = db.Select("select count(ID) from expences where  Year(date)='" + combo_year.Text + "'");
            txt_totl_sales.Text = cou.Tables["tbl1"].Rows[0][0].ToString();
            DataSet cash = db.Select("select SUM(amount) from expences where Year(date)='" + combo_year.Text + "'");
            txt_tot_cash.Text = cash.Tables["tbl1"].Rows[0][0].ToString();
        }

        private void combo_year_SelectedValueChanged(object sender, EventArgs e)
        {
            if (combo_month.Text == "January")
            {
                a = 01;
                month_ser();
            }
            else if (combo_month.Text == "February")
            {
                a = 02;
                month_ser();
            }
            else if (combo_month.Text == "March")
            {
                a = 03;
                month_ser();
            }
            else if (combo_month.Text == "April")
            {
                a = 04;
                month_ser();
            }
            else if (combo_month.Text == "May")
            {
                a = 05;
                month_ser();
            }
            else if (combo_month.Text == "June")
            {
                a = 06;
                month_ser();
            }
            else if (combo_month.Text == "July")
            {
                a = 07;
                month_ser();
            }
            else if (combo_month.Text == "Agust")
            {
                a = 08;
                month_ser();
            }
            else if (combo_month.Text == "September")
            {
                a = 09;
                month_ser();
            }
            else if (combo_month.Text == "October")
            {
                a = 10;
                month_ser();
            }
            else if (combo_month.Text == "November")
            {
                a = 11;
                month_ser();
            }
            else
            {
                a = 12;
                month_ser();
            }
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {
            string sql = "select * from Expences  where ID like '%" + txt_search.Text + "%'";
            dataGridView1.DataSource = db.list_process(sql);
        }
    }
}
