﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bakery_Management_System
{
    public partial class Recipes : UserControl
    {

        public Recipes()
        {
            InitializeComponent();
        }
        //****+++++++++++ DATABASE CONNECTION **+++++++++
        DB_Connecctions db = new DB_Connecctions();
        //****+++++++ LIST VIEW ON LOAD  ******+++++++++++
        public void list_view()
        {
            listView1.View = View.Details;
            listView1.GridLines = true;
            listView1.FullRowSelect = true;
            listView1.Columns.Add("Item Name", 150);
            listView1.Columns.Add("Item Quantity", 100);
            listView1.Columns.Add("Unit", 100);    
        }
        //**** Deals LIST ////
        public void get_deals()
        {
            DataSet dba = db.Select("select * from Deals where status=1 and Recipe=0");
            if (dba != null && dba.Tables["tbl1"].Rows.Count > 0)
            {
                combo_search.DataSource = dba.Tables["tbl1"].DefaultView;
                combo_search.DisplayMember = "Deal_Name";
                combo_search.ValueMember = "Deal_ID";
            }
        }
        public void Get_items()
        {
            DataSet dba = db.Select("select * from Items");
            if (dba != null && dba.Tables["tbl1"].Rows.Count > 0)
            {
                combo_item.DataSource = dba.Tables["tbl1"].DefaultView;
                combo_item.DisplayMember = "Item_Name";
            }
        }
        //*** GET ID ///
        public void get_ID()
        {
            DataSet ds = db.Select("select isnull(max(cast(recipe_ID as int)),0)+1 from recipes");
            txt_item_Code.Text = ds.Tables["tbl1"].Rows[0][0].ToString();
        }
        //**** ADD IN LIST VIEW //*+
        public void add_list_View()
        {
            try
            {
                string[] row = { combo_item.Text, txt_quantity.Text, txt_Unit.Text };
                var listViewItem = new ListViewItem(row);
                listView1.Items.Add(listViewItem);
                txt_quantity.Clear();
                combo_item.Focus();
            }
            catch (Exception)
            {

            }
        }
        //****** LIST VIEW RECIPWER
        public void list_Box()
        {
            DataSet ds = db.Select("select Distinct(Recipe_ID) from Recipes where status=1");
            if (ds.Tables["tbl1"].Rows.Count > 0)
            {
                this.listBox1.DataSource = ds.Tables["tbl1"].DefaultView;
                this.listBox1.DisplayMember = "Recipe_ID";

            }
        }
        private void Recipes_Load(object sender, EventArgs e)
        {
            get_ID();// Get ID
            list_view(); /// list view
            get_deals(); /// Get deals Name
            Get_items();// Get Items List
            list_Box();// List Boxte
        }
        //*** ADD DETAIL BUTTON
        private void button1_Click(object sender, EventArgs e)
        {
            add_list_View();
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            
            //try
            //{
                if (listView1.Items.Count>0)
                {
                    DataSet ds = db.Select("select * from recipes where Recipe_ID='"+txt_item_Code.Text+"'");
                    if (ds.Tables["tbl1"].Rows.Count>0)
                    {
                        foreach (ListViewItem items in listView1.Items)
                        {
                            db.ExecuteQuery("Update Recipes set Ingredient='" + items.SubItems[0].Text + "',Quantity='" + items.SubItems[1].Text + "',Unit='" + items.SubItems[2].Text + "' where recipe_ID='"+txt_item_Code.Text+"'");
                        }
                        MessageBox.Show("Recipe Updated Succefully.", "Recipe Section", MessageBoxButtons.OK, MessageBoxIcon.Information);     
                    }
                    else
                    {
                        foreach (ListViewItem items in listView1.Items)
                    {
                        db.ExecuteQuery("insert into Recipes(Recipe_ID,Deal_ID,Ingredient,Quantity,Unit,Status) values('" + txt_item_Code.Text + "','" + combo_search.SelectedValue + "','" + items.SubItems[0].Text + "','" + items.SubItems[1].Text + "','" + items.SubItems[2].Text + "',1)");
                        db.ExecuteQuery("Update Deals set Recipe=1 where Deal_ID='"+combo_search.SelectedValue+"'");
                    }
                    MessageBox.Show("New Recipe Saved Succefully.", "Recipe Section", MessageBoxButtons.OK, MessageBoxIcon.Information); 
                    }
                    
                }
            //}
            //catch (Exception)
            //{
               
            //}
        }
        //**** get detail of deal on the base of deal name
        private void combo_search_SelectedIndexChanged(object sender, EventArgs e)
        {
           DataSet ds= db.Select("select Deal_Detail from deals where deal_name='"+combo_search.Text+"'");
            if(ds.Tables["tbl1"].Rows.Count>0)
            {
                txt_deal_detail.Text = ds.Tables["tbl1"].Rows[0]["Deal_Detail"].ToString();
            }
        }

        private void combo_item_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = db.Select("select Unit from Items where Item_Name='" + combo_item.Text + "'");
            if (ds.Tables["tbl1"].Rows.Count > 0)
            {
                txt_Unit.Text = ds.Tables["tbl1"].Rows[0]["unit"].ToString();
            }
        }

        private void listView1_Click(object sender, EventArgs e)
        {
            listView1.FocusedItem.Remove();
        }
    }
}
