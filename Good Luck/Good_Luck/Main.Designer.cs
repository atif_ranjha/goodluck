﻿namespace Bakery_Management_System
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(main));
            this.main_panel = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.catogryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dealsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.receipeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.expencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.departmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.designationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_panel
            // 
            this.main_panel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.main_panel.AutoSize = true;
            this.main_panel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.main_panel.Location = new System.Drawing.Point(28, 96);
            this.main_panel.Name = "main_panel";
            this.main_panel.Size = new System.Drawing.Size(1233, 552);
            this.main_panel.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.PowderBlue;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.catogryToolStripMenuItem,
            this.dealsToolStripMenuItem,
            this.saleToolStripMenuItem,
            this.receipeToolStripMenuItem,
            this.itemToolStripMenuItem,
            this.logManagerToolStripMenuItem,
            this.expencesToolStripMenuItem,
            this.accountToolStripMenuItem,
            this.employeeToolStripMenuItem,
            this.departmentToolStripMenuItem,
            this.designationToolStripMenuItem,
            this.changeUserToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.exitToolStripMenuItem,
            this.exitToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1284, 93);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // catogryToolStripMenuItem
            // 
            this.catogryToolStripMenuItem.Image = global::Bakery_Management_System.Properties.Resources.Categorize_64px;
            this.catogryToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.catogryToolStripMenuItem.Name = "catogryToolStripMenuItem";
            this.catogryToolStripMenuItem.Size = new System.Drawing.Size(103, 89);
            this.catogryToolStripMenuItem.Text = "Categories";
            this.catogryToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.catogryToolStripMenuItem.Click += new System.EventHandler(this.catogryToolStripMenuItem_Click);
            // 
            // dealsToolStripMenuItem
            // 
            this.dealsToolStripMenuItem.Image = global::Bakery_Management_System.Properties.Resources.Restaurant_Menu_64px2;
            this.dealsToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.dealsToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.dealsToolStripMenuItem.Name = "dealsToolStripMenuItem";
            this.dealsToolStripMenuItem.Size = new System.Drawing.Size(76, 89);
            this.dealsToolStripMenuItem.Text = "Deals";
            this.dealsToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.dealsToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.dealsToolStripMenuItem.Click += new System.EventHandler(this.dealsToolStripMenuItem_Click);
            // 
            // saleToolStripMenuItem
            // 
            this.saleToolStripMenuItem.Image = global::Bakery_Management_System.Properties.Resources.Land_Sales_64px;
            this.saleToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.saleToolStripMenuItem.Name = "saleToolStripMenuItem";
            this.saleToolStripMenuItem.Size = new System.Drawing.Size(76, 89);
            this.saleToolStripMenuItem.Text = "Sale";
            this.saleToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.saleToolStripMenuItem.Click += new System.EventHandler(this.saleToolStripMenuItem_Click);
            // 
            // receipeToolStripMenuItem
            // 
            this.receipeToolStripMenuItem.Image = global::Bakery_Management_System.Properties.Resources.Fast_Food_Drive_Thru_64px;
            this.receipeToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.receipeToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.receipeToolStripMenuItem.Name = "receipeToolStripMenuItem";
            this.receipeToolStripMenuItem.Size = new System.Drawing.Size(76, 89);
            this.receipeToolStripMenuItem.Text = "POS";
            this.receipeToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.receipeToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.receipeToolStripMenuItem.Click += new System.EventHandler(this.receipeToolStripMenuItem_Click);
            // 
            // itemToolStripMenuItem
            // 
            this.itemToolStripMenuItem.Image = global::Bakery_Management_System.Properties.Resources.List_64px;
            this.itemToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.itemToolStripMenuItem.Name = "itemToolStripMenuItem";
            this.itemToolStripMenuItem.Size = new System.Drawing.Size(76, 89);
            this.itemToolStripMenuItem.Text = "Items";
            this.itemToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.itemToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.itemToolStripMenuItem.Visible = false;
            this.itemToolStripMenuItem.Click += new System.EventHandler(this.itemToolStripMenuItem_Click);
            // 
            // logManagerToolStripMenuItem
            // 
            this.logManagerToolStripMenuItem.Image = global::Bakery_Management_System.Properties.Resources.Bulleted_List_64px;
            this.logManagerToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.logManagerToolStripMenuItem.Name = "logManagerToolStripMenuItem";
            this.logManagerToolStripMenuItem.Size = new System.Drawing.Size(80, 89);
            this.logManagerToolStripMenuItem.Text = "Reports";
            this.logManagerToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.logManagerToolStripMenuItem.Click += new System.EventHandler(this.logManagerToolStripMenuItem_Click);
            // 
            // expencesToolStripMenuItem
            // 
            this.expencesToolStripMenuItem.Image = global::Bakery_Management_System.Properties.Resources.Paper_Money_64px;
            this.expencesToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.expencesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.expencesToolStripMenuItem.Name = "expencesToolStripMenuItem";
            this.expencesToolStripMenuItem.Size = new System.Drawing.Size(93, 89);
            this.expencesToolStripMenuItem.Text = "Expences";
            this.expencesToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.expencesToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.expencesToolStripMenuItem.Click += new System.EventHandler(this.expencesToolStripMenuItem_Click);
            // 
            // accountToolStripMenuItem
            // 
            this.accountToolStripMenuItem.Image = global::Bakery_Management_System.Properties.Resources.Admin_Settings_Male_64px;
            this.accountToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.accountToolStripMenuItem.Name = "accountToolStripMenuItem";
            this.accountToolStripMenuItem.Size = new System.Drawing.Size(85, 89);
            this.accountToolStripMenuItem.Text = "Account";
            this.accountToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.accountToolStripMenuItem.Click += new System.EventHandler(this.accountToolStripMenuItem_Click);
            // 
            // employeeToolStripMenuItem
            // 
            this.employeeToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("employeeToolStripMenuItem.Image")));
            this.employeeToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.employeeToolStripMenuItem.Name = "employeeToolStripMenuItem";
            this.employeeToolStripMenuItem.Size = new System.Drawing.Size(105, 89);
            this.employeeToolStripMenuItem.Text = "Employees";
            this.employeeToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.employeeToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.employeeToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.employeeToolStripMenuItem.Click += new System.EventHandler(this.employeeToolStripMenuItem_Click);
            // 
            // departmentToolStripMenuItem
            // 
            this.departmentToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("departmentToolStripMenuItem.Image")));
            this.departmentToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.departmentToolStripMenuItem.Name = "departmentToolStripMenuItem";
            this.departmentToolStripMenuItem.Size = new System.Drawing.Size(121, 89);
            this.departmentToolStripMenuItem.Text = "Departments";
            this.departmentToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.departmentToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.departmentToolStripMenuItem.Click += new System.EventHandler(this.departmentToolStripMenuItem_Click);
            // 
            // designationToolStripMenuItem
            // 
            this.designationToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("designationToolStripMenuItem.Image")));
            this.designationToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.designationToolStripMenuItem.Name = "designationToolStripMenuItem";
            this.designationToolStripMenuItem.Size = new System.Drawing.Size(115, 89);
            this.designationToolStripMenuItem.Text = "Designation";
            this.designationToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.designationToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.designationToolStripMenuItem.Click += new System.EventHandler(this.designationToolStripMenuItem_Click);
            // 
            // changeUserToolStripMenuItem
            // 
            this.changeUserToolStripMenuItem.Image = global::Bakery_Management_System.Properties.Resources.Change_User_64px;
            this.changeUserToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.changeUserToolStripMenuItem.Name = "changeUserToolStripMenuItem";
            this.changeUserToolStripMenuItem.Size = new System.Drawing.Size(118, 89);
            this.changeUserToolStripMenuItem.Text = "Change User";
            this.changeUserToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.changeUserToolStripMenuItem.Click += new System.EventHandler(this.changeUserToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Image = global::Bakery_Management_System.Properties.Resources.Questions_64px;
            this.helpToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(76, 89);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = global::Bakery_Management_System.Properties.Resources.Info_64px;
            this.exitToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(76, 89);
            this.exitToolStripMenuItem.Text = "About";
            this.exitToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Image = global::Bakery_Management_System.Properties.Resources.Exit_64px;
            this.exitToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(76, 89);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click_1);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.ClientSize = new System.Drawing.Size(1284, 651);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.main_panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "main";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Good Luck";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.main_FormClosing);
            this.Load += new System.EventHandler(this.main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel main_panel;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem itemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dealsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem expencesToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem catogryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem receipeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem departmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem designationToolStripMenuItem;
    }
}

